package au.net.causal.graalah.tools;

import au.net.causal.graalah.tools.ClassSelector.ClassInPackagesSelector;
import au.net.causal.graalah.tools.ClassSelector.ClassesAnnotatedWithSelector;
import au.net.causal.graalah.tools.ClassSelector.ClassesWithAnyMemberAnnotatedWithSelector;
import au.net.causal.graalah.tools.ClassSelector.SubclassesSelector;
import au.net.causal.graalah.tools.p1.ClassInP1;
import au.net.causal.graalah.tools.p2.ClassInP2;
import com.oracle.svm.hosted.FeatureImpl.FeatureAccessImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TestClassSelector
{
    @Nested
    class Base
    {
        @Mock
        private FeatureAccessImpl featureAccess;

        @BeforeEach
        private void setUpFeatureAccessMock()
        {
            lenient().when(featureAccess.findSubclasses(eq(Animal.class))).thenReturn(
                    Arrays.asList(Animal.class, Cat.class, Dog.class));
            lenient().when(featureAccess.findAnnotatedClasses(eq(MyAnnotation.class))).thenReturn(
                    Collections.singletonList(Galah.class));
            lenient().when(featureAccess.findAnnotatedClasses(eq(MyOtherAnnotation.class))).thenReturn(
                    Arrays.asList(Galah.class, Cockatoo.class));
            lenient().when(featureAccess.findSubclasses(eq(Object.class))).thenReturn(
                    Arrays.asList(TestClassSelector.class, ClassInP1.class, ClassInP2.class,
                                  Cat.class, Dog.class, Animal.class,
                                  MyAnnotation.class, MyOtherAnnotation.class,
                                  Galah.class, Cockatoo.class, Magpie.class, Ibis.class));
        }

        @Test
        void classesInPackage()
        {
            ClassSelector s = ClassSelector.select(featureAccess).classesInPackageOf(ClassInP2.class);
            assertThat(s.stream()).containsExactly(ClassInP2.class);
        }

        @Test
        void subclasses()
        {
            ClassSelector s = ClassSelector.select(featureAccess).subclassesOf(Animal.class);
            assertThat(s.stream()).containsExactlyInAnyOrder(Cat.class, Dog.class);
        }

        @Test
        void annotatedClasses()
        {
            ClassSelector s = ClassSelector.select(featureAccess).classesAnnotatedWith(MyAnnotation.class);
            assertThat(s.stream()).containsExactly(Galah.class);
        }

        @Test
        void classesWithAnnotatedMembers()
        {
            ClassSelector s = ClassSelector.select(featureAccess)
                                           .classesWithAnyMembersAnnotatedWith(MyOtherAnnotation.class);
            assertThat(s.stream()).containsExactlyInAnyOrder(Cockatoo.class, Ibis.class);
        }
    }

    @Nested
    class ClassInPackages
    {
        @Test
        void filtersClassesInPackages()
        {
            ClassInPackagesSelector p2Selector = new ClassInPackagesSelector(
                    Collections.singleton("au.net.causal.graalah.tools.p2"));

            List<Class<?>> classes = Arrays.asList(TestClassSelector.class, ClassInP1.class, ClassInP2.class);

            assertThat(classes.stream().filter(p2Selector.asFilter())).containsExactly(ClassInP2.class);
        }

        @Test
        void basePackageDoesNotIncludeChildPackages()
        {
            ClassInPackagesSelector p2Selector = new ClassInPackagesSelector(
                    Collections.singleton("au.net.causal.graalah.tools"));

            List<Class<?>> classes = Arrays.asList(TestClassSelector.class, ClassInP1.class, ClassInP2.class);

            assertThat(classes.stream().filter(p2Selector.asFilter())).containsExactly(TestClassSelector.class);
        }
    }

    @Nested
    class Subclasses
    {
        @Test
        void findsSubclasses()
        {
            SubclassesSelector selector = new SubclassesSelector(Collections.singleton(Animal.class));

            List<Class<?>> classes = Arrays.asList(Animal.class, Cat.class, Dog.class, TestClassSelector.class);

            assertThat(classes.stream().filter(selector.asFilter())).containsExactlyInAnyOrder(Cat.class, Dog.class);
        }

        @Test
        void findsEverythingForObject()
        {
            SubclassesSelector selector = new SubclassesSelector(Collections.singleton(Object.class));

            List<Class<?>> classes = Arrays.asList(Animal.class, Cat.class, Dog.class, TestClassSelector.class);

            assertThat(classes.stream().filter(selector.asFilter())).containsExactlyInAnyOrder(
                    Cat.class, Dog.class, Animal.class, TestClassSelector.class);
        }
    }

    @Nested
    class Annotated
    {
        @Test
        void findsSingleAnnotatedClass()
        {
            ClassesAnnotatedWithSelector selector =
                    new ClassesAnnotatedWithSelector(Collections.singleton(MyAnnotation.class));

            List<Class<?>> classes = Arrays.asList(Galah.class, Cockatoo.class, Magpie.class, Ibis.class,
                                                   MyAnnotation.class, MyOtherAnnotation.class,
                                                   TestClassSelector.class);

            assertThat(classes.stream().filter(selector.asFilter())).containsExactly(Galah.class);
        }

        @Test
        void findsMultipleAnnotatedClasses()
        {
            ClassesAnnotatedWithSelector selector =
                    new ClassesAnnotatedWithSelector(Collections.singleton(MyOtherAnnotation.class));

            List<Class<?>> classes = Arrays.asList(Galah.class, Cockatoo.class, Magpie.class, Ibis.class,
                                                   MyAnnotation.class, MyOtherAnnotation.class,
                                                   TestClassSelector.class);

            assertThat(classes.stream().filter(selector.asFilter()))
                    .containsExactlyInAnyOrder(Galah.class, Cockatoo.class);
        }
    }

    @Nested
    class AnnotatedMembers
    {
        @Test
        void findsSingleClassWithAnnotatedMember()
        {
            ClassesWithAnyMemberAnnotatedWithSelector selector =
                    new ClassesWithAnyMemberAnnotatedWithSelector(Collections.singleton(MyAnnotation.class));

            List<Class<?>> classes = Arrays.asList(Galah.class, Cockatoo.class, Magpie.class, Ibis.class,
                                                   MyAnnotation.class, MyOtherAnnotation.class,
                                                   TestClassSelector.class);

            assertThat(classes.stream().filter(selector.asFilter()))
                    .containsExactlyInAnyOrder(Magpie.class);
        }

        @Test
        void findsMultipleClassesWithAnnotatedMembers()
        {
            ClassesWithAnyMemberAnnotatedWithSelector selector =
                    new ClassesWithAnyMemberAnnotatedWithSelector(Collections.singleton(MyOtherAnnotation.class));

            List<Class<?>> classes = Arrays.asList(Galah.class, Cockatoo.class, Magpie.class, Ibis.class,
                                                   MyAnnotation.class, MyOtherAnnotation.class,
                                                   TestClassSelector.class);

            assertThat(classes.stream().filter(selector.asFilter()))
                    .containsExactlyInAnyOrder(Cockatoo.class, Ibis.class);
        }
    }

    private static class Animal
    {
    }

    private static class Cat extends Animal
    {
    }

    private static class Dog extends Animal
    {
    }

    @Retention(RetentionPolicy.RUNTIME)
    private static @interface MyAnnotation
    {
    }

    @Retention(RetentionPolicy.RUNTIME)
    private static @interface MyOtherAnnotation
    {
    }

    @MyAnnotation
    @MyOtherAnnotation
    private static class Galah
    {
    }

    @MyOtherAnnotation
    private static class Cockatoo
    {
        private final String name;

        @MyOtherAnnotation
        private Cockatoo(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }

    private static class Magpie
    {
        @MyAnnotation
        private String name;
    }

    private static class Ibis
    {
        @MyOtherAnnotation
        @Override
        public String toString()
        {
            return "Ibis";
        }
    }
}
