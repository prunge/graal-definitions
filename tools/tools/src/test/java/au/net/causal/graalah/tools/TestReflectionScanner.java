package au.net.causal.graalah.tools;

import au.net.causal.graalah.tools.ReflectionScanner.Configuration;
import au.net.causal.graalah.tools.ReflectionScanner.ConstructorParameterAnalyzer;
import au.net.causal.graalah.tools.ReflectionScanner.FieldsAnalyzer;
import au.net.causal.graalah.tools.ReflectionScanner.InnerClassesAnalyzer;
import au.net.causal.graalah.tools.ReflectionScanner.MethodParameterAnalyzer;
import au.net.causal.graalah.tools.ReflectionScanner.MethodReturnAnalyzer;
import au.net.causal.graalah.tools.ReflectionScanner.TypeHandling;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.*;

class TestReflectionScanner
{
    @Nested
    class Builder
    {
        @Test
        void testNonNested()
        {
            ReflectionScanner scanner = new ReflectionScanner.Builder().scanningAllContent().build();
            Set<Class<?>> results = scanner.scan(MySimpleBean.class);
            assertThat(results).containsExactlyInAnyOrder(MySimpleBean.class, Key.class, Value.class, Something.class);
        }

        @Test
        void testNested()
        {
            ReflectionScanner scanner = new ReflectionScanner.Builder().scanningAllContent().build();
            Set<Class<?>> results = scanner.scan(MyContainerBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyContainerBean.class,
                                                          MyContainerBean.MyInnerBean.class,
                                                          MyContainerBean.MyNestedBean.class,
                                                          Key.class,
                                                          Value.class);
        }
    }

    @Nested
    class Fields
    {
        @Test
        void simpleFields()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new FieldsAnalyzer()));
            Set<Class<?>> results = scanner.scan(MySimpleBean.class);
            assertThat(results).containsExactlyInAnyOrder(MySimpleBean.class, Value.class);
        }

        @Test
        void rawScannerWithGenericFields()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.RAW),
                                                              Collections.singleton(new FieldsAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class);
        }

        @Test
        void genericScannerWithGenericFields()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new FieldsAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class, Key.class, Value.class);
        }
    }

    @Nested
    class MethodReturns
    {
        @Test
        void simpleMethodReturns()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new MethodReturnAnalyzer()));
            Set<Class<?>> results = scanner.scan(MySimpleBean.class);
            assertThat(results).containsExactlyInAnyOrder(MySimpleBean.class, Value.class);
        }

        @Test
        void rawScannerWithGenericReturn()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.RAW),
                                                              Collections.singleton(new MethodReturnAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class, Something.class);
        }

        @Test
        void genericScannerWithGenericReturn()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new MethodReturnAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class, Key.class, Value.class, Something.class);
        }
    }

    @Nested
    class MethodParameters
    {
        @Test
        void simpleMethodParameters()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new MethodParameterAnalyzer()));
            Set<Class<?>> results = scanner.scan(MySimpleBean.class);
            assertThat(results).containsExactlyInAnyOrder(MySimpleBean.class, Key.class);
        }

        @Test
        void rawScannerWithGenericParameters()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.RAW),
                                                              Collections.singleton(new MethodParameterAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class, Something.class);
        }

        @Test
        void genericScannerWithGenericParameters()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new MethodParameterAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class, Value.class, Something.class);
        }
    }

    @Nested
    class ConstructorParameters
    {
        @Test
        void simpleConstructorParameters()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new ConstructorParameterAnalyzer()));
            Set<Class<?>> results = scanner.scan(MySimpleBean.class);
            assertThat(results).containsExactlyInAnyOrder(MySimpleBean.class, Something.class, Key.class);
        }

        @Test
        void rawScannerWithGenericParameters()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.RAW),
                                                              Collections.singleton(new ConstructorParameterAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class, Something.class);
        }

        @Test
        void genericScannerWithGenericParameters()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new ConstructorParameterAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyMapBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyMapBean.class, Key.class, Something.class);
        }
    }

    @Nested
    class InnerClasses
    {
        @Test
        void innerClasses()
        {
            ReflectionScanner scanner = new ReflectionScanner(new Configuration(TypeHandling.GENERICS),
                                                              Collections.singleton(new InnerClassesAnalyzer()));
            Set<Class<?>> results = scanner.scan(MyContainerBean.class);
            assertThat(results).containsExactlyInAnyOrder(MyContainerBean.class,
                                                          MyContainerBean.MyNestedBean.class,
                                                          MyContainerBean.MyInnerBean.class);
        }
    }

    @Nested
    class TestTypeHandling
    {
        @Test
        void rawWithSimpleField() throws ReflectiveOperationException
        {
            class MyClass
            {
                private Number value;
            }
            Field value = MyClass.class.getDeclaredField("value");

            Set<Class<?>> results = new LinkedHashSet<>();
            TypeHandling.RAW.analyzeType(value.getType(), value.getGenericType(), results);

            assertThat(results).containsExactlyInAnyOrder(Number.class);
        }

        @Test
        void rawWithMapField() throws ReflectiveOperationException
        {
            class MyClass
            {
                private Map<Number, Value> map;
            }
            Field map = MyClass.class.getDeclaredField("map");

            Set<Class<?>> results = new LinkedHashSet<>();
            TypeHandling.RAW.analyzeType(map.getType(), map.getGenericType(), results);

            assertThat(results).containsExactlyInAnyOrder(Map.class);
        }

        @Test
        void genericsWithSimpleField() throws ReflectiveOperationException
        {
            class MyClass
            {
                private Number value;
            }
            Field value = MyClass.class.getDeclaredField("value");

            Set<Class<?>> results = new LinkedHashSet<>();
            TypeHandling.GENERICS.analyzeType(value.getType(), value.getGenericType(), results);

            assertThat(results).containsExactlyInAnyOrder(Number.class);
        }

        @Test
        void genericsWithMapField() throws ReflectiveOperationException
        {
            class MyClass
            {
                private Map<Number, Value> map;
            }
            Field map = MyClass.class.getDeclaredField("map");

            Set<Class<?>> results = new LinkedHashSet<>();
            TypeHandling.GENERICS.analyzeType(map.getType(), map.getGenericType(), results);

            assertThat(results).containsExactlyInAnyOrder(Map.class, Number.class, Value.class);
        }

        @Test
        void genericsWithDeepNesting() throws ReflectiveOperationException
        {
            class MyClass
            {
                private Map<List<Number>, Map<Key, Value>> map;
            }
            Field map = MyClass.class.getDeclaredField("map");

            Set<Class<?>> results = new LinkedHashSet<>();
            TypeHandling.GENERICS.analyzeType(map.getType(), map.getGenericType(), results);

            assertThat(results).containsExactlyInAnyOrder(Map.class, List.class, Number.class, Key.class, Value.class);
        }
    }

    private static abstract class MyContainerBean
    {
        private static abstract class MyNestedBean
        {
            private Key key;
            private Value value;
        }

        private abstract class MyInnerBean
        {
            private Key key;
        }
    }

    private static abstract class MySimpleBean
    {
        private MySimpleBean(Something something, Key key)
        {
        }

        private Value value;

        protected abstract Value aMethod();
        protected abstract void voidMethod(Key key);
    }

    private static abstract class MyMapBean
    {
        private MyMapBean(List<Key> keys, Something something)
        {
        }

        private Map<? extends Key, ? extends Value> myMap;

        protected abstract Map<? extends Key, ? extends Value> aMethod();
        protected abstract void voidMethod(Collection<Value> values, Something some);
        private Something something()
        {
            return null;
        }
    }

    private static class Key
    {
    }

    private static class Value
    {
    }

    private static interface Something
    {
    }
}
