package au.net.causal.graalah.tools;

public interface SubstitutionFeature
{
    public boolean isSubstitutionClassEnabled(Class<?> substitutionClass, String originalClassName);
}
