package au.net.causal.graalah.tools;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

public class ReflectionScanner
{
    private final Collection<? extends Analyzer> analyzers;
    private final Configuration configuration;
    private final Predicate<? super Class<?>> filter;

    public ReflectionScanner()
    {
        this(defaultConfiguration());
    }

    public ReflectionScanner(Configuration configuration)
    {
        this(configuration, defaultAnalyzers());
    }

    private static Configuration defaultConfiguration()
    {
        return new Configuration(TypeHandling.GENERICS);
    }

    private static Collection<? extends Analyzer> defaultAnalyzers()
    {
        return Arrays.asList(
                new FieldsAnalyzer(),
                new MethodReturnAnalyzer(),
                new MethodParameterAnalyzer(),
                new ConstructorParameterAnalyzer(),
                new InnerClassesAnalyzer()
        );
    }

    protected ReflectionScanner(Configuration configuration, Collection<? extends Analyzer> analyzers)
    {
        this.analyzers = new ArrayList<>(analyzers);
        this.configuration = Objects.requireNonNull(configuration);
        this.filter = ReflectionScanner::defaultFilter;
    }

    private static final List<String> DEFAULT_EXCLUDE_PREFIXES = Arrays.asList(
            "java.",
            "javax.",
            "org.w3c.dom.",
            "sun.misc.",
            "jdk.internal."
    );

    private static boolean defaultFilter(Class<?> clazz)
    {
        return !clazz.isPrimitive() &&
               DEFAULT_EXCLUDE_PREFIXES.stream().noneMatch(excludePrefix -> clazz.getName().startsWith(excludePrefix));
    }

    public Set<Class<?>> scan(Class<?> clazz)
    {
        Set<Class<?>> alreadyAnalyzed = new LinkedHashSet<>();
        Set<Class<?>> newClassesCollected = new LinkedHashSet<>();
        Set<Class<?>> classesToAnalyze = new LinkedHashSet<>();
        classesToAnalyze.add(clazz);

        //Keep analyzing until a round yields no new classes to scan
        while (!classesToAnalyze.isEmpty())
        {
            //Perform an analysis
            for (Class<?> classToAnalyze : classesToAnalyze)
            {
                analyze(classToAnalyze, newClassesCollected);
            }

            //We have now analyzed these classes
            alreadyAnalyzed.addAll(classesToAnalyze);

            //Filter out the classes already processed from the new set
            newClassesCollected.removeAll(classesToAnalyze);
            newClassesCollected.removeAll(alreadyAnalyzed);
            newClassesCollected.removeIf(filter.negate());

            //Prepare for next round
            classesToAnalyze = newClassesCollected;
            newClassesCollected = new LinkedHashSet<>();
        }

        return alreadyAnalyzed;
    }

    private void analyze(Class<?> clazz, Set<? super Class<?>> collector)
    {
        for (Analyzer analyzer : analyzers)
        {
            analyzer.analyze(clazz, collector, configuration);
        }
    }

    public static class Builder
    {
        //Instances of known analyzers we can use for building, singletons for the builder so we can add them to
        //analyzer set without duplication

        private static final FieldsAnalyzer FIELDS_ANALYZER = new FieldsAnalyzer();
        private static final MethodReturnAnalyzer METHOD_RETURN_ANALYZER = new MethodReturnAnalyzer();
        private static final MethodParameterAnalyzer METHOD_PARAMETER_ANALYZER = new MethodParameterAnalyzer();
        private static final ConstructorParameterAnalyzer CONSTRUCTOR_PARAMETER_ANALYZER = new ConstructorParameterAnalyzer();
        private static final InnerClassesAnalyzer INNER_CLASSES_ANALYZER = new InnerClassesAnalyzer();

        private final Set<Analyzer> analyzers = new LinkedHashSet<>();
        private TypeHandling typeHandling = TypeHandling.GENERICS;

        public Builder notScanningInsideGenerics()
        {
            return withTypeHandling(TypeHandling.RAW);
        }

        public Builder scanningInsideGenerics()
        {
            return withTypeHandling(TypeHandling.GENERICS);
        }

        public Builder withTypeHandling(TypeHandling typeHandling)
        {
            this.typeHandling = Objects.requireNonNull(typeHandling);
            return this;
        }

        public Builder scanningFields()
        {
            return withAnalyzer(FIELDS_ANALYZER);
        }

        public Builder scanningMethodParameters()
        {
            return withAnalyzer(METHOD_PARAMETER_ANALYZER);
        }

        public Builder scanningMethodReturns()
        {
            return withAnalyzer(METHOD_RETURN_ANALYZER);
        }

        public Builder scanningMethods()
        {
            return scanningMethodParameters().scanningMethodReturns();
        }

        public Builder scanningConstructorParameters()
        {
            return withAnalyzer(CONSTRUCTOR_PARAMETER_ANALYZER);
        }

        public Builder scanningInnerClasses()
        {
            return withAnalyzer(INNER_CLASSES_ANALYZER);
        }

        public Builder scanningAllMembers()
        {
            return scanningFields().scanningMethods().scanningConstructorParameters();
        }

        public Builder scanningAllContent()
        {
            return scanningAllMembers().scanningInnerClasses();
        }

        public Builder withAnalyzer(Analyzer analyzer)
        {
            this.analyzers.add(Objects.requireNonNull(analyzer));
            return this;
        }

        public ReflectionScanner build()
        {
            return new ReflectionScanner(new Configuration(typeHandling), analyzers);
        }
    }

    public static class Configuration
    {
        private final TypeHandling typeHandling;

        public Configuration(TypeHandling typeHandling)
        {
            this.typeHandling = typeHandling;
        }

        public TypeHandling getTypeHandling()
        {
            return typeHandling;
        }
    }

    public static enum TypeHandling
    {
        RAW
        {
            @Override
            public void analyzeType(Class<?> rawType, Type genericType, Set<? super Class<?>> collector)
            {
                //Not interested in arrays, just components
                while (rawType.isArray())
                {
                    rawType = rawType.getComponentType();
                }

                collector.add(rawType);
            }
        },
        GENERICS
        {
            private void analyzeTypeLocal(Type genericType, Set<? super Class<?>> collector,
                                     Set<? super Type> alreadyProcessed)
            {
                boolean newType = alreadyProcessed.add(genericType);
                if (!newType)
                    return;

                //Pick apart the generic type

                if (genericType instanceof Class<?>)
                    RAW.analyzeType((Class<?>)genericType, genericType, collector);
                else if (genericType instanceof ParameterizedType)
                {
                    //Raw type
                    ParameterizedType pType = (ParameterizedType) genericType;
                    collector.add((Class<?>) pType.getRawType());

                    //Parameters
                    for (Type typeArgument : pType.getActualTypeArguments())
                    {
                        analyzeTypeLocal(typeArgument, collector, alreadyProcessed);
                    }
                }
                else if (genericType instanceof TypeVariable<?>)
                {
                    TypeVariable<?> tv = (TypeVariable<?>) genericType;
                    for (Type bound : tv.getBounds())
                    {
                        analyzeTypeLocal(bound, collector, alreadyProcessed);
                    }
                }
                else if (genericType instanceof WildcardType)
                {
                    WildcardType w = (WildcardType) genericType;
                    for (Type lowerBound : w.getLowerBounds())
                    {
                        analyzeTypeLocal(lowerBound, collector, alreadyProcessed);
                    }
                    for (Type upperBound : w.getUpperBounds())
                    {
                        analyzeTypeLocal(upperBound, collector, alreadyProcessed);
                    }
                }
                else if (genericType instanceof GenericArrayType)
                {
                    GenericArrayType ga = (GenericArrayType) genericType;
                    analyzeTypeLocal(ga.getGenericComponentType(), collector, alreadyProcessed);
                }
                else
                    throw new Error("Unknown generic type: " + genericType);
            }

            @Override
            public void analyzeType(Class<?> rawType, Type genericType, Set<? super Class<?>> collector)
            {
                analyzeTypeLocal(genericType, collector, new HashSet<>());
            }
        };

        public abstract void analyzeType(Class<?> rawType, Type genericType, Set<? super Class<?>> collector);
    }

    protected static interface Analyzer
    {
        public void analyze(Class<?> clazz, Set<? super Class<?>> collector, Configuration configuration);
    }

    protected static class FieldsAnalyzer implements Analyzer
    {
        @Override
        public void analyze(Class<?> clazz, Set<? super Class<?>> collector, Configuration configuration)
        {
            for (Field field : clazz.getDeclaredFields())
            {
                analyzeField(field, collector, configuration);
            }
        }

        protected void analyzeField(Field field, Set<? super Class<?>> collector, Configuration configuration)
        {
            configuration.getTypeHandling().analyzeType(field.getType(), field.getGenericType(), collector);
        }
    }

    protected static class MethodReturnAnalyzer implements Analyzer
    {
        @Override
        public void analyze(Class<?> clazz, Set<? super Class<?>> collector, Configuration configuration)
        {
            for (Method method : clazz.getDeclaredMethods())
            {
                analyzeMethodReturn(method, collector, configuration);
            }
        }

        protected void analyzeMethodReturn(Method method, Set<? super Class<?>> collector, Configuration configuration)
        {
            if (method.getReturnType() != null && method.getReturnType() != void.class)
                configuration.getTypeHandling().analyzeType(method.getReturnType(), method.getGenericReturnType(), collector);
        }
    }

    protected static abstract class ExecutableParameterAnalyzer implements Analyzer
    {
        protected void analyzeParameters(Executable executable, Set<? super Class<?>> collector, Configuration configuration)
        {
            for (Parameter parameter : executable.getParameters())
            {
                configuration.getTypeHandling().analyzeType(parameter.getType(), parameter.getParameterizedType(), collector);
            }
        }
    }

    protected static class MethodParameterAnalyzer extends ExecutableParameterAnalyzer
    {
        @Override
        public void analyze(Class<?> clazz, Set<? super Class<?>> collector, Configuration configuration)
        {
            for (Method method : clazz.getDeclaredMethods())
            {
                analyzeParameters(method, collector, configuration);
            }
        }
    }

    protected static class ConstructorParameterAnalyzer extends ExecutableParameterAnalyzer
    {
        @Override
        public void analyze(Class<?> clazz, Set<? super Class<?>> collector, Configuration configuration)
        {
            for (Constructor<?> constructor : clazz.getDeclaredConstructors())
            {
                analyzeParameters(constructor, collector, configuration);
            }
        }
    }

    protected static class InnerClassesAnalyzer implements Analyzer
    {
        @Override
        public void analyze(Class<?> clazz, Set<? super Class<?>> collector, Configuration configuration)
        {
            for (Class<?> innerClass : clazz.getDeclaredClasses())
            {
                //No generics for innerClass, but we might as well re-use the other logic in TypeHandling
                configuration.getTypeHandling().analyzeType(innerClass, innerClass, collector);
            }
        }
    }
}
