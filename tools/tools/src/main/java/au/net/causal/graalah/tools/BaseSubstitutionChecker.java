package au.net.causal.graalah.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;

public abstract class BaseSubstitutionChecker<T> implements Predicate<String>
{
    private final Class<T> target;

    protected BaseSubstitutionChecker(Class<T> target)
    {
        this.target = Objects.requireNonNull(target);
    }

    public Class<T> getTarget()
    {
        return target;
    }

    @Override
    public boolean test(String originalClassName)
    {
        return substitutionFeatures.stream().anyMatch(f -> f.isSubstitutionClassEnabled(target, originalClassName));
    }

    private static final Collection<SubstitutionFeature> substitutionFeatures = new ArrayList<>();

    public static void registerSubstitutionFeature(SubstitutionFeature substitutionFeature)
    {
        substitutionFeatures.add(substitutionFeature);
    }
}
