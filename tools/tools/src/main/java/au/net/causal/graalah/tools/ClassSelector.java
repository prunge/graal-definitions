package au.net.causal.graalah.tools;

import com.oracle.svm.hosted.FeatureImpl;
import com.oracle.svm.hosted.FeatureImpl.FeatureAccessImpl;
import org.graalvm.nativeimage.hosted.Feature;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClassSelector
{
    private final FeatureImpl.FeatureAccessImpl featureAccess;
    private final List<? extends Selector> selectors;
    private final Predicate<? super Class<?>> filter;

    public ClassSelector(FeatureImpl.FeatureAccessImpl featureAccess, List<? extends Selector> selectors,
                         Predicate<? super Class<?>> filter)
    {
        this.featureAccess = Objects.requireNonNull(featureAccess);
        this.selectors = Objects.requireNonNull(selectors);
        this.filter = Objects.requireNonNull(filter);
    }

    public ClassSelector(FeatureImpl.FeatureAccessImpl featureAccess, List<? extends Selector> selectors)
    {
        this(featureAccess, selectors, clazz -> true);
    }

    private static void logWarning(String message, Throwable ex)
    {
        System.err.println("Warning: " + message + ": " + ex);
    }

    public static SelectorBuilder select(Feature.FeatureAccess featureAccess)
    {
        if (!(featureAccess instanceof FeatureImpl.FeatureAccessImpl))
            throw new RuntimeException("Only " + FeatureImpl.FeatureAccessImpl.class.getName() + " is supported");

        return new SelectorBuilder((FeatureImpl.FeatureAccessImpl)featureAccess);
    }

    public ClassSelector excluding(Predicate<? super Class> excludeFilter)
    {
        return new ClassSelector(featureAccess, selectors, excludeFilter.negate());
    }

    public Stream<Class<?>> stream()
    {
        if (selectors.isEmpty())
            throw new IllegalStateException("Class selector must have at least one selector");

        Iterator<? extends Selector> i = selectors.iterator();
        Selector firstSelector = i.next();
        Stream<Class<?>> stream = firstSelector.select(featureAccess);

        //Apply explicit filter
        stream = stream.filter(filter);

        //Apply subsequent selectors as filters
        while (i.hasNext())
        {
            stream = stream.filter(i.next().asFilter());
        }

        return stream;
    }

    public static class SelectorBuilder
    {
        private final FeatureImpl.FeatureAccessImpl featureAccess;

        public SelectorBuilder(FeatureAccessImpl featureAccess)
        {
            this.featureAccess = Objects.requireNonNull(featureAccess);
        }

        public ClassSelector classesInPackageOf(Class<?> clazz)
        {
            return classesInPackagesOf(clazz);
        }

        public ClassSelector classesInPackagesOf(Class<?>... classes)
        {
            return classesInPackages(Stream.of(classes)
                                           .map(ReflectionTools::packageNameOfClass)
                                           .toArray(String[]::new));
        }

        public ClassSelector classesInPackages(String... packageNames)
        {
            return new ClassSelector(featureAccess, Collections.singletonList(new ClassInPackagesSelector(
                                                                                    Arrays.asList(packageNames))));
        }

        public ClassSelector subclassesOf(Class<?>... classes)
        {
            return new ClassSelector(featureAccess, Collections.singletonList(new SubclassesSelector(
                                                                                    Arrays.asList(classes))));
        }

        public ClassSelector classesAnnotatedWith(Collection<? extends Class<? extends Annotation>> annotations)
        {
            return new ClassSelector(featureAccess, Collections.singletonList(new ClassesAnnotatedWithSelector(annotations)));
        }

        @SafeVarargs
        public final ClassSelector classesAnnotatedWith(Class<? extends Annotation>... annotations)
        {
            return classesAnnotatedWith(Arrays.asList(annotations));
        }

        @SafeVarargs
        public final ClassSelector classesWithAnyMembersAnnotatedWith(Class<? extends Annotation>... annotations)
        {
            return new ClassSelector(featureAccess, Collections.singletonList(new ClassesWithAnyMemberAnnotatedWithSelector(
                                                                                    Arrays.asList(annotations))));
        }
    }

    protected static interface Selector
    {
        public default Stream<Class<?>> select(FeatureImpl.FeatureAccessImpl featureAccess)
        {
            return featureAccess.findSubclasses(Object.class)
                                .stream()
                                .filter(asFilter());
        }

        public Predicate<Class<?>> asFilter();
    }

    protected static class ClassInPackagesSelector implements Selector
    {
        private final Collection<String> packageNames;

        protected ClassInPackagesSelector(Collection<String> packageNames)
        {
            this.packageNames = Objects.requireNonNull(packageNames);
        }

        @Override
        public Predicate<Class<?>> asFilter()
        {
            return clazz -> packageNames.contains(ReflectionTools.packageNameOfClass(clazz));
        }
    }

    protected static class SubclassesSelector implements Selector
    {
        private final Collection<? extends Class<?>> baseClasses;

        public SubclassesSelector(Collection<? extends Class<?>> baseClasses)
        {
            this.baseClasses = Objects.requireNonNull(baseClasses);
        }

        @Override
        public Stream<Class<?>> select(FeatureAccessImpl featureAccess)
        {
            Set<Class<?>> allSubclasses = new LinkedHashSet<>();
            for (Class<?> baseClass : baseClasses)
            {
                allSubclasses.addAll(featureAccess.findSubclasses(baseClass));
            }
            allSubclasses.removeAll(baseClasses);

            return allSubclasses.stream();
        }

        @Override
        public Predicate<Class<?>> asFilter()
        {
            return clazz -> !baseClasses.contains(clazz)
                    && baseClasses.stream().anyMatch(baseClass -> baseClass.isAssignableFrom(clazz));
        }
    }

    protected static class ClassesAnnotatedWithSelector implements Selector
    {
        private final Collection<? extends Class<? extends Annotation>> annotations;

        public ClassesAnnotatedWithSelector(Collection<? extends Class<? extends Annotation>> annotations)
        {
            this.annotations = Objects.requireNonNull(annotations);
        }

        @Override
        public Stream<Class<?>> select(FeatureAccessImpl featureAccess)
        {
            Set<Class<?>> foundClasses = new LinkedHashSet<>();
            for (Class<? extends Annotation> annotation : annotations)
            {
                foundClasses.addAll(featureAccess.findAnnotatedClasses(annotation));
            }

            return foundClasses.stream();
        }

        @Override
        public Predicate<Class<?>> asFilter()
        {
            //Intentionally not processing container annotations to match what featureAccess does
            return clazz -> annotations.stream().anyMatch(clazz::isAnnotationPresent);
        }
    }

    protected abstract static class ClassesWithMembersAnnotatedWithSelector implements Selector
    {
        private final Collection<? extends Class<? extends Annotation>> annotations;

        public ClassesWithMembersAnnotatedWithSelector(Collection<? extends Class<? extends Annotation>> annotations)
        {
            this.annotations = Objects.requireNonNull(annotations);
        }

        protected abstract Collection<? extends AccessibleObject> membersOfClassToScan(Class<?> clazz);

        @Override
        public Predicate<Class<?>> asFilter()
        {
            return clazz ->
            {
                for (AccessibleObject member : membersOfClassToScan(clazz))
                {
                    if (annotations.stream().anyMatch(member::isAnnotationPresent))
                        return true;
                }

                //No matching annotations on any member
                return false;
            };
        }
    }

    protected static class ClassesWithAnyMemberAnnotatedWithSelector extends ClassesWithMembersAnnotatedWithSelector
    {
        public ClassesWithAnyMemberAnnotatedWithSelector(Collection<? extends Class<? extends Annotation>> annotations)
        {
            super(annotations);
        }

        @Override
        protected Collection<? extends AccessibleObject> membersOfClassToScan(Class<?> clazz)
        {
            List<AccessibleObject> members = new ArrayList<>();

            try
            {
                members.addAll(Arrays.asList(clazz.getDeclaredConstructors()));
            }
            catch (NoClassDefFoundError e)
            {
               logWarning("Failed to read constructors for " + clazz, e);
            }

            try
            {
                members.addAll(Arrays.asList(clazz.getDeclaredFields()));
            }
            catch (NoClassDefFoundError e)
            {
                logWarning("Failed to read fields for " + clazz, e);
            }

            try
            {
                members.addAll(Arrays.asList(clazz.getDeclaredMethods()));
            }
            catch (NoClassDefFoundError e)
            {
                logWarning("Failed to read methods for " + clazz, e);
            }

            return members;
        }
    }

    public static class FilterBuilder
    {
        private final FeatureImpl.FeatureAccessImpl featureAccess;
        private final Selector selector;
        private final List<Predicate<Class<?>>> filters = new ArrayList<>();

        protected FilterBuilder(FeatureImpl.FeatureAccessImpl featureAccess, Selector selector)
        {
            this.featureAccess = Objects.requireNonNull(featureAccess);
            this.selector = Objects.requireNonNull(selector);
        }

        public Collection<Class<?>> build()
        {
            return selector.select(this.featureAccess).filter(and(filters)).collect(Collectors.toList());
        }
    }

    private static <T> Predicate<T> and(Collection<? extends Predicate<? super T>> predicates)
    {
        return value -> {
          for (Predicate<? super T> predicate : predicates)
          {
              if (!predicate.test(value))
                  return false;
          }

          return true;
        };
    }
}

