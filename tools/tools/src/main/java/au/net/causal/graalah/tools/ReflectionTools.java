package au.net.causal.graalah.tools;

class ReflectionTools
{
    static String packageNameOfClass(Class<?> c)
    {
        //Copied from Java 9's Class.packageName()
        //If this project ever has minimum JDK version >= 9 then use that and ditch this code
        String pn;
        while (c.isArray())
        {
            c = c.getComponentType();
        }
        if (c.isPrimitive())
            pn = "java.lang";
        else
        {
            String cn = c.getName();
            int dot = cn.lastIndexOf('.');
            pn = (dot != -1) ? cn.substring(0, dot).intern() : "";
        }

        return pn;
    }
}
