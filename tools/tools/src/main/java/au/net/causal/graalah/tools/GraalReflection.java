package au.net.causal.graalah.tools;

import org.graalvm.nativeimage.hosted.RuntimeReflection;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GraalReflection
{
    private final Collection<? extends Class<?>> classes;

    private boolean allFields;
    private boolean allMethods;
    private boolean allConstructors;

    protected GraalReflection(Collection<? extends Class<?>> classes)
    {
        //Safe to not copy because only we can call this
        this.classes = classes;
    }

    public static GraalReflection with(Class<?>... classes)
    {
        return with(Arrays.asList(classes));
    }

    public static GraalReflection with(Collection<? extends Class<?>> classes)
    {
        return new GraalReflection(classes);
    }

    public static GraalReflection with(Stream<? extends Class<?>> classes)
    {
        return new GraalReflection(classes.collect(Collectors.toList()));
    }

    public GraalReflection allFields()
    {
        this.allFields = true;
        return this;
    }

    public GraalReflection allConstructors()
    {
        this.allConstructors = true;
        return this;
    }

    public GraalReflection allMethods()
    {
        this.allMethods = true;
        return this;
    }

    public GraalReflection allMembers()
    {
        return allConstructors().allFields().allMethods();
    }

    public void register()
    {
        for (Class<?> clazz : classes)
        {
            RuntimeReflection.register(clazz);

            if (allFields)
                RuntimeReflection.register(clazz.getDeclaredFields());
            if (allMethods)
                RuntimeReflection.register(clazz.getDeclaredMethods());
            if (allConstructors)
                RuntimeReflection.register(clazz.getDeclaredConstructors());
        }
    }
}
