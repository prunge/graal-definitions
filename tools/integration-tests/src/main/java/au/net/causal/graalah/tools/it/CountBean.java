package au.net.causal.graalah.tools.it;

public class CountBean
{
    private int count;

    public CountBean()
    {
    }

    public CountBean(int count)
    {
        this.count = count;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }
}
