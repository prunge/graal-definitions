package au.net.causal.graalah.tools.it;

import au.net.causal.graalah.tools.GraalReflection;
import com.oracle.svm.core.annotate.AutomaticFeature;
import org.graalvm.nativeimage.hosted.Feature;

@AutomaticFeature
public class GraalConfig implements Feature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        GraalReflection.with(NameBean.class, CountBean.class).allMembers().register();
    }
}
