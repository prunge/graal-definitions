package au.net.causal.graalah.tools.it;

import com.fasterxml.jackson.annotation.JsonCreator;

public class NameBean
{
    private final String name;

    @JsonCreator
    public NameBean(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
