package au.net.causal.graalah.tools.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.common.io.Resources;

import java.io.IOError;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class App
{
    //Intentionally load resources ahead of time
    //When building a Graal image these are read at build-time, meaning we don't have to deal with Graal's
    //runtime reflection configuration

    private static final String nameJson = readResource("name.json");
    private static final String countJson = readResource("count.json");

    private static String readResource(String name)
    {
        try
        {
            return Resources.toString(App.class.getResource("/" + name), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            throw new IOError(e);
        }
    }

    public static void main(String... args)
    throws IOException
    {
        ObjectMapper om = new ObjectMapper();
        om.registerModule(new ParameterNamesModule());

        Properties output = new Properties();

        NameBean value = om.readValue(nameJson, NameBean.class);
        output.setProperty("name", value.getName());

        CountBean b2 = om.readValue(countJson, CountBean.class);
        output.setProperty("count", String.valueOf(b2.getCount()));

        output.store(System.out, null);
    }
}
