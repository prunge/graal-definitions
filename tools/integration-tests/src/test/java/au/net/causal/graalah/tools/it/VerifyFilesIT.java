package au.net.causal.graalah.tools.it;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

class VerifyFilesIT
{
    private static final Properties dataProperties = new Properties();

    @BeforeAll
    private static void setUpFiles()
    throws IOException
    {
        Path dataFile = Paths.get(System.getProperty("data.file", "target/out-java.properties"));
        System.out.println("Verifying " + dataFile.toAbsolutePath().toString());
        dataProperties.clear();
        try (InputStream is = Files.newInputStream(dataFile))
        {
            dataProperties.load(is);
        }
    }

    @Test
    void dataFileHasCorrectContent()
    {
        assertThat(dataProperties).contains(entry("name", "John Galah"));
        assertThat(dataProperties).contains(entry("count", "75"));
    }
}
