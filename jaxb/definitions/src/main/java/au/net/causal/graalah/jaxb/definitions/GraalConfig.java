package au.net.causal.graalah.jaxb.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import au.net.causal.graalah.tools.ClassSelector;
import au.net.causal.graalah.tools.GraalReflection;
import au.net.causal.graalah.tools.SubstitutionFeature;
import com.oracle.svm.core.jdk.proxy.DynamicProxyRegistry;
import com.sun.xml.bind.v2.ContextFactory;
import com.sun.xml.bind.v2.model.annotation.Locatable;
import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.hosted.Feature;

import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttachmentRef;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlInlineBinaryData;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSchemaTypes;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.TreeSet;

public abstract class GraalConfig implements Feature, SubstitutionFeature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        List<Class<? extends Annotation>> jaxbAnnotations = Arrays.asList(
                XmlAccessorOrder.class,
                XmlAccessorType.class,
                XmlAnyAttribute.class,
                XmlAnyElement.class,
                XmlAttachmentRef.class,
                XmlAttribute.class,
                XmlElement.class,
                XmlElementDecl.class,
                XmlElementRef.class,
                XmlElementRefs.class,
                XmlElementWrapper.class,
                XmlElements.class,
                XmlEnum.class,
                XmlEnumValue.class,
                XmlID.class,
                XmlIDREF.class,
                XmlInlineBinaryData.class,
                XmlJavaTypeAdapter.class,
                XmlList.class,
                XmlMimeType.class,
                XmlMixed.class,
                XmlNs.class,
                XmlRegistry.class,
                XmlRootElement.class,
                XmlSchema.class,
                XmlSchemaType.class,
                XmlSeeAlso.class,
                XmlSchemaTypes.class,
                XmlTransient.class,
                XmlType.class,
                XmlValue.class
        );

        GraalReflection.with(ContextFactory.class).allMethods().register();
        GraalReflection.with(jaxbAnnotations).allMethods().register();

        DynamicProxyRegistry proxyRegistry = ImageSingletons.lookup(DynamicProxyRegistry.class);
        for (Class<? extends Annotation> jaxbAnnotation : jaxbAnnotations)
        {
            proxyRegistry.addProxyClass(jaxbAnnotation, Locatable.class);
        }

        //Need to be able to reflectively create some collection classes
        //See com.sun.xml.bind.v2.runtime.reflect.Lister.COLLECTION_IMPL_CLASSES
        GraalReflection.with(ArrayList.class,
                             LinkedList.class,
                             HashSet.class,
                             TreeSet.class,
                             Stack.class)
                       .allConstructors().register();

        //User classes
        //TODO some of these can't be used on classes
        GraalReflection.with(ClassSelector.select(access).classesAnnotatedWith(jaxbAnnotations).stream()).allMembers().register();

        GraalReflection.with(ClassSelector.select(access).subclassesOf(XmlAdapter.class).stream()).allMembers().register();
    }

    @Override
    public boolean isSubstitutionClassEnabled(Class<?> substitutionClass, String originalClassName)
    {
        if (substitutionClass == PropertyFactory.class)
            return isSubstitutionEnabledForPropertyFactory();
        else
            return false;
    }

    /**
     * Opt-in/out for substitution class.
     *
     * @see com.sun.xml.bind.v2.runtime.property.PropertyFactory
     */
    protected abstract boolean isSubstitutionEnabledForPropertyFactory();

    @Override
    public void afterRegistration(AfterRegistrationAccess access)
    {
        BaseSubstitutionChecker.registerSubstitutionFeature(this);
    }
}
