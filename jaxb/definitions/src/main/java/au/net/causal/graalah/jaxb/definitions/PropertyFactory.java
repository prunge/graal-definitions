package au.net.causal.graalah.jaxb.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;
import com.sun.xml.bind.v2.model.core.PropertyKind;
import com.sun.xml.bind.v2.model.runtime.RuntimeAttributePropertyInfo;
import com.sun.xml.bind.v2.model.runtime.RuntimeElementPropertyInfo;
import com.sun.xml.bind.v2.model.runtime.RuntimePropertyInfo;
import com.sun.xml.bind.v2.model.runtime.RuntimeValuePropertyInfo;
import com.sun.xml.bind.v2.runtime.JAXBContextImpl;
import com.sun.xml.bind.v2.runtime.property.AttributeProperty;
import com.sun.xml.bind.v2.runtime.property.Property;
import com.sun.xml.bind.v2.runtime.property.ValueProperty;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@TargetClass(value = com.sun.xml.bind.v2.runtime.property.PropertyFactory.class, onlyWith = PropertyFactory.Checker.class)
public final class PropertyFactory
{
    @Alias
    private static Constructor<? extends Property>[] propImpls = null;

    //Mostly copy+paste from the original except for the c.setAccessible(true)
    @Substitute
    public static Property create(JAXBContextImpl grammar, RuntimePropertyInfo info ) {

        PropertyKind kind = info.kind();

        switch(kind) {
            case ATTRIBUTE:
                return new AttributeProperty(grammar, (RuntimeAttributePropertyInfo)info);
            case VALUE:
                return new ValueProperty(grammar, (RuntimeValuePropertyInfo)info);
            case ELEMENT:
                if(((RuntimeElementPropertyInfo)info).isValueList())
                    return (Property)(Object)(new ListElementProperty(grammar, (RuntimeElementPropertyInfo) info));
                break;
            case REFERENCE:
            case MAP:
                break;
            default:
                assert false;
        }


        boolean isCollection = info.isCollection();
        boolean isLeaf = isLeaf(info);

        Constructor<? extends Property> c = propImpls[(isLeaf ? 0 : 6)+(isCollection ? 3 : 0)+kind.propertyIndex];
        try {
            c.setAccessible(true); //Added since Graal 19.1.0 seems to need it (is more restrictive with reflection?)
            return c.newInstance( grammar, info );
        } catch (InstantiationException e) {
            throw new InstantiationError(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new IllegalAccessError(e.getMessage());
        } catch (InvocationTargetException e) {
            Throwable t = e.getCause();
            if(t instanceof Error)
                throw (Error)t;
            if(t instanceof RuntimeException)
                throw (RuntimeException)t;

            throw new AssertionError(t);
        }
    }

    @Alias
    static boolean isLeaf(RuntimePropertyInfo info)
    {
        throw new Error("Alias only");
    }

    public static class Checker extends BaseSubstitutionChecker<PropertyFactory>
    {
        public Checker()
        {
            super(PropertyFactory.class);
        }
    }
}

/**
 * No substitutions, this exists just so we can access this class from PropertyFactory.
 */
@TargetClass(className = "com.sun.xml.bind.v2.runtime.property.ListElementProperty")
final class ListElementProperty<BeanT,ListT,ItemT>
{
    @Alias
    public ListElementProperty(JAXBContextImpl grammar, RuntimeElementPropertyInfo prop)
    {
        throw new Error("Alias only");
    }
}
