package au.net.causal.graalah.jaxb.feature;

import au.net.causal.graalah.jaxb.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
    @Override
    protected boolean isSubstitutionEnabledForPropertyFactory()
    {
        return true;
    }
}
