package au.net.causal.graalah.jaxb.it;

import au.net.causal.graal_definitions.it.jaxb.bird.Bird;
import au.net.causal.graal_definitions.it.jaxb.bird.OwnerListType;
import au.net.causal.graal_definitions.it.jaxb.bird.Species;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

public class App
{
    public static void main(String... args)
    throws IOException, JAXBException, DatatypeConfigurationException
    {
        Properties output = new Properties();
        JAXBContext jaxb = JAXBContext.newInstance(Bird.class);
        Marshaller marshaller = jaxb.createMarshaller();
        Unmarshaller unmarshaller = jaxb.createUnmarshaller();
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();

        Bird bird = new Bird();
        bird.setName("Squinky");
        bird.setSpecies(Species.GALAH);
        OwnerListType owners = new OwnerListType();
        owners.getOwner().add("John Galah");
        bird.setOwners(owners);
        bird.setDateOfBirth(datatypeFactory.newXMLGregorianCalendarDate(1977, 1, 9, DatatypeConstants.FIELD_UNDEFINED));

        //Generate XML
        StringWriter buf = new StringWriter();
        marshaller.marshal(bird, buf);
        output.setProperty("bird.xml", buf.toString());

        //Parse XML, override name and write back out
        Bird parsedBird = unmarshaller.unmarshal(new StreamSource(new StringReader(buf.toString())), Bird.class).getValue();
        parsedBird.setName(parsedBird.getName() + " Junior");
        StringWriter buf2 = new StringWriter();
        marshaller.marshal(parsedBird, buf2);
        output.setProperty("bird.reserialized.xml", buf2.toString());

        //Write properties data out
        output.store(System.out, null);
    }
}
