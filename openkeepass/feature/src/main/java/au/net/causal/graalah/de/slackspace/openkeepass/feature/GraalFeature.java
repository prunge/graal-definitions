package au.net.causal.graalah.de.slackspace.openkeepass.feature;

import au.net.causal.graalah.de.slackspace.openkeepass.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
}
