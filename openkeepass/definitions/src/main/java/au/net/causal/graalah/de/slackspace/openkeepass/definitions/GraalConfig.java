package au.net.causal.graalah.de.slackspace.openkeepass.definitions;

import au.net.causal.graalah.tools.ClassSelector;
import au.net.causal.graalah.tools.GraalReflection;
import de.slackspace.openkeepass.domain.xml.adapter.BooleanSimpleXmlAdapter;
import de.slackspace.openkeepass.domain.xml.adapter.ByteSimpleXmlAdapter;
import de.slackspace.openkeepass.domain.xml.adapter.CalendarSimpleXmlAdapter;
import de.slackspace.openkeepass.domain.xml.adapter.PropertyValueXmlAdapter;
import de.slackspace.openkeepass.domain.xml.adapter.UUIDSimpleXmlAdapter;
import org.graalvm.nativeimage.hosted.Feature;
import org.simpleframework.xml.Root;

public abstract class GraalConfig implements Feature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        //All Simple XML annotated classes
        GraalReflection.with(
            ClassSelector.select(access)
                         .classesAnnotatedWith(Root.class)
                         .stream()
        ).allMembers().register();

        //OpenKeepass Simple XML support classes
        GraalReflection.with(
                BooleanSimpleXmlAdapter.class,
                ByteSimpleXmlAdapter.class,
                CalendarSimpleXmlAdapter.class,
                PropertyValueXmlAdapter.class,
                UUIDSimpleXmlAdapter.class
        ).allMembers().register();

        //To support Simple XML itself
        try
        {
            Class<?> templateLabel = Class.forName("org.simpleframework.xml.core.TemplateLabel"); //Non-public class
            GraalReflection.with(
                ClassSelector.select(access).subclassesOf(templateLabel).stream()
            ).allMembers().register();
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e);
        }
    }
}
