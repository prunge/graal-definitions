package au.net.causal.graalah.de.slackspace.openkeepass.it;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class VerifyFilesIT
{
    private static final Properties dataProperties = new Properties();

    @BeforeAll
    private static void setUpFiles()
    throws IOException
    {
        Path dataFile = Paths.get(System.getProperty("data.file", "target/out-java.properties"));
        System.out.println("Verifying " + dataFile.toAbsolutePath().toString());
        dataProperties.clear();
        try (InputStream is = Files.newInputStream(dataFile))
        {
            dataProperties.load(is);
        }
    }

    @Test
    void dataFileHasCorrectContent()
    {
        assertThat(dataProperties).containsEntry("Galah.username", "galah");
        assertThat(dataProperties).containsEntry("Galah.password", "mypassword");
    }
}
