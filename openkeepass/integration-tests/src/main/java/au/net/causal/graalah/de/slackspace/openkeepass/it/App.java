package au.net.causal.graalah.de.slackspace.openkeepass.it;

import com.google.common.io.Resources;
import de.slackspace.openkeepass.KeePassDatabase;
import de.slackspace.openkeepass.domain.Entry;
import de.slackspace.openkeepass.domain.KeePassFile;

import java.io.ByteArrayInputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class App
{
    //Intentionally load resources ahead of time
    //When building a Graal image these are read at build-time, meaning we don't have to deal with Graal's
    //runtime reflection configuration

    private static final byte[] keepassDbFile = readResource("test.kdbx");
    private static final String keepassDbPassword = "test";

    private static byte[] readResource(String name)
    {
        try
        {
            return Resources.toByteArray(App.class.getResource("/" + name));
        }
        catch (IOException e)
        {
            throw new IOError(e);
        }
    }

    public static void main(String... args)
    throws IOException
    {
        Properties output = new Properties();

        try (InputStream is = new ByteArrayInputStream(keepassDbFile))
        {
            KeePassFile keepassDb = KeePassDatabase.getInstance(is).openDatabase(keepassDbPassword);
            for (Entry entry : keepassDb.getEntries())
            {
                output.setProperty(entry.getTitle() + ".username", entry.getUsername());
                output.setProperty(entry.getTitle() + ".password", entry.getPassword());
            }
        }

        output.store(System.out, null);
    }
}
