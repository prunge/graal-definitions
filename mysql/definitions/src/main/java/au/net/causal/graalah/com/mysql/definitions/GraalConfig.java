package au.net.causal.graalah.com.mysql.definitions;

import au.net.causal.graalah.tools.ClassSelector;
import au.net.causal.graalah.tools.GraalReflection;
import com.mysql.cj.conf.ConnectionUrl;
import com.mysql.cj.exceptions.CJCommunicationsException;
import com.mysql.cj.exceptions.CJException;
import com.mysql.cj.exceptions.ConnectionIsClosedException;
import com.mysql.cj.exceptions.InvalidConnectionAttributeException;
import com.mysql.cj.exceptions.RSAException;
import com.mysql.cj.exceptions.SSLParamsException;
import com.mysql.cj.exceptions.UnableToConnectException;
import com.mysql.cj.exceptions.WrongArgumentException;
import com.mysql.cj.jdbc.AbandonedConnectionCleanupThread;
import com.mysql.cj.jdbc.ClientInfoProvider;
import com.mysql.cj.jdbc.ha.LoadBalanceExceptionChecker;
import com.mysql.cj.log.Jdk14Logger;
import com.mysql.cj.log.NullLogger;
import com.mysql.cj.log.ProfilerEventHandler;
import com.mysql.cj.log.Slf4JLogger;
import com.mysql.cj.log.StandardLogger;
import com.mysql.cj.protocol.SocketFactory;
import com.mysql.cj.util.TimeUtil;
import com.oracle.svm.core.configure.ResourcesRegistry;
import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.hosted.Feature;
import org.graalvm.nativeimage.impl.RuntimeClassInitializationSupport;

import java.sql.BatchUpdateException;

public abstract class GraalConfig implements Feature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        ImageSingletons.lookup(RuntimeClassInitializationSupport.class).rerunInitialization(AbandonedConnectionCleanupThread.class, "required");

        //Find all these by searching for usages of ExceptionFactory.createException()
        GraalReflection.with(
                WrongArgumentException.class,
                UnableToConnectException.class,
                RSAException.class,
                InvalidConnectionAttributeException.class,
                CJException.class,
                BatchUpdateException.class,
                SSLParamsException.class,
                ConnectionIsClosedException.class,
                CJCommunicationsException.class
        ).allMembers().register();

        //Also register everything that is used by Util.getInstance()
        GraalReflection.with(ClassSelector.select(access).subclassesOf(
                        ConnectionUrl.class,
                        SocketFactory.class,
                        ClientInfoProvider.class,
                        ProfilerEventHandler.class,
                        LoadBalanceExceptionChecker.class).stream()
        ).allMembers().register();

        //Register all Log implementations except SLF4J logger
        GraalReflection.with(StandardLogger.class, NullLogger.class, Jdk14Logger.class).allMembers().register();

        //Detect if SLF4J is available and register that one conditionally
        Class<?> slf4jLoggerClass = access.findClassByName("org.slf4j.Logger");
        if (slf4jLoggerClass != null)
            GraalReflection.with(Slf4JLogger.class).register();

        //Register and prime the time zone mappings
        //This will make the TimeZoneMappings actually be loaded at build time, but load the TimeZoneMapping.properties
        //just in case it's needed later also
        addResource("com/mysql/cj/util/TimeZoneMapping.properties");
        TimeUtil.getCanonicalTimezone("Etc/GMT", null);
    }

    protected void addResource(String name)
    {
        ImageSingletons.lookup(ResourcesRegistry.class).addResources(name);
    }

}
