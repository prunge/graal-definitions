package au.net.causal.graalah.com.mysql.feature;

import au.net.causal.graalah.com.mysql.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
}
