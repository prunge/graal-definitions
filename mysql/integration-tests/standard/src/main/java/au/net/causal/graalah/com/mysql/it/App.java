package au.net.causal.graalah.com.mysql.it;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class App
{
    public static void main(String... args)
    throws SQLException, IOException
    {
        String jdbcUrl = args[0];
        String user = args[1];
        String password = args[2];

        Properties output = new Properties();

        try (Connection con = DriverManager.getConnection(jdbcUrl, user, password))
        {
            output.setProperty("db.productName", con.getMetaData().getDatabaseProductName());

            List<String> cats = new ArrayList<>();

            try (PreparedStatement stat = con.prepareStatement("select name from cat order by name");
                 ResultSet rs = stat.executeQuery())
            {
                while (rs.next())
                {
                    cats.add(rs.getString(1));
                }
            }
            output.setProperty("cats", String.join(" ", cats));

            try (PreparedStatement stat = con.prepareStatement("show status like 'Ssl_version'");
                 ResultSet rs = stat.executeQuery())
            {
                if (rs.next())
                {
                    output.setProperty("ssl.version", rs.getString(2));
                }
            }
        }

        output.store(System.out, null);
    }
}
