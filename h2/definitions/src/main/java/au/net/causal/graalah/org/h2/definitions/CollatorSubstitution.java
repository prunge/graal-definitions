package au.net.causal.graalah.org.h2.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Makes Collator.getAvailableLocales() return a list of locales built at compile time rather than crashing out.
 *
 * See: https://github.com/oracle/graal/issues/839
 *
 * <p>
 *
 * This is not a complete solution as Graal currently does not support I18N properly, not loading resource bundles
 * from multiple locales and only ever returning the default collator for the root locale regardless of which locale
 * is requested.  This substitution will allow Collator.getAvailableLocales() to not crash, but the list of
 * locales returned are only those that use the default collator of the root locale since this is the only one that
 * Graal returns from Collator.getInstance() anyway.
 */
@TargetClass(value=Collator.class, onlyWith=CollatorSubstitution.Checker.class)
public final class CollatorSubstitution
{
    @Substitute
    public static Locale[] getAvailableLocales()
    {
        return CollatorStatics.availableLocales.clone();
    }

    public static class Checker extends BaseSubstitutionChecker<CollatorSubstitution>
    {
        public Checker()
        {
            super(CollatorSubstitution.class);
        }
    }
}

final class CollatorStatics
{
    /**
     * Since Graal only seems to ever return the default collator at runtime, we can only deem to support
     * the locales that have this default collator.   This is evaluated at image build time.
     */
    static final Locale[] availableLocales = findLocalesWithEqualCollator(Collator.getInstance(Locale.ROOT));

    private static Locale[] findLocalesWithEqualCollator(Collator collator)
    {
        List<Locale> matchingLocales = new ArrayList<>();
        for (Locale locale : Collator.getAvailableLocales())
        {
            Collator c = Collator.getInstance(locale);
            if (collator.equals(c))
                matchingLocales.add(locale);
        }

        return matchingLocales.toArray(new Locale[0]);
    }
}
