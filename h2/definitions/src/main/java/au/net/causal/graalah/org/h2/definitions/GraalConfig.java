package au.net.causal.graalah.org.h2.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import au.net.causal.graalah.tools.ClassSelector;
import au.net.causal.graalah.tools.GraalReflection;
import au.net.causal.graalah.tools.SubstitutionFeature;
import org.graalvm.nativeimage.hosted.Feature;
import org.graalvm.nativeimage.hosted.RuntimeClassInitialization;
import org.h2.api.TableEngine;
import org.h2.store.fs.FilePathNio;

public abstract class GraalConfig implements Feature, SubstitutionFeature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        RuntimeClassInitialization.initializeAtRunTime(access.findClassByName("org.h2.store.fs.FileNioMemData"));
        GraalReflection.with(FilePathNio.class).allConstructors().register();
        GraalReflection.with(ClassSelector.select(access).subclassesOf(TableEngine.class).stream()).allConstructors().register();
    }

    @Override
    public boolean isSubstitutionClassEnabled(Class<?> substitutionClass, String originalClassName)
    {
        if (substitutionClass == CollatorSubstitution.class)
            return isSubstitutionEnabledForCollator();
        else
            return false;
    }

    /**
     * Opt-in/out for substitution class.
     *
     * @see CollatorSubstitution
     */
    protected abstract boolean isSubstitutionEnabledForCollator();

    @Override
    public void afterRegistration(AfterRegistrationAccess access)
    {
        BaseSubstitutionChecker.registerSubstitutionFeature(this);
    }
}
