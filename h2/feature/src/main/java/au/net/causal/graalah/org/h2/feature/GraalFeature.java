package au.net.causal.graalah.org.h2.feature;

import au.net.causal.graalah.org.h2.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
    @Override
    protected boolean isSubstitutionEnabledForCollator()
    {
        return true;
    }
}
