package au.net.causal.graalah.org.h2.it;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class VerifyDatabaseIT
{
    /**
     * Output from executable when creating a new database.
     */
    private static final Properties dataProperties = new Properties();

    /**
     * Output from executable when loading an existing database on disk.
     */
    private static final Properties dataPropertiesAgain = new Properties();

    @BeforeAll
    private static void setUpAllProperties()
    throws IOException
    {
        readProperties(dataProperties, "data.file", "target/out-java.properties");
        readProperties(dataPropertiesAgain, "data.file.again", "target/out-java-again.properties");
    }

    private static void readProperties(Properties target, String fileNameSystemProperty, String defaultFileName)
    throws IOException
    {
        Path dataFile = Paths.get(System.getProperty(fileNameSystemProperty, defaultFileName));
        System.out.println("Verifying " + dataFile.toAbsolutePath().toString());
        target.clear();
        try (InputStream is = Files.newInputStream(dataFile))
        {
            target.load(is);
        }
    }

    private void performCommonChecks(Properties properties)
    {
        assertThat(properties).containsEntry("db.productName", "H2");
        assertThat(properties).containsEntry("cats", "Dinah-Kah Janvier");

        //There should be at least a few collations in the list (as opposed to _just_ the root locale)
        assertThat(properties).hasEntrySatisfying("collations", value -> assertThat(value.toString().length())
                                                                            .describedAs("Collation list should be a decent size")
                                                                            .isGreaterThan(100));
    }

    @Test
    void dataFileHasCorrectContent()
    {
        performCommonChecks(dataProperties);
        assertThat(dataProperties).containsEntry("table.created", "true");
    }

    @Test
    void reloadDataFileHasCorrectContent()
    {
        performCommonChecks(dataPropertiesAgain);
        assertThat(dataPropertiesAgain).doesNotContainKey("table.created");
    }
}
