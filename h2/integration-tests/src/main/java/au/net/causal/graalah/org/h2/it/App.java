package au.net.causal.graalah.org.h2.it;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class App
{
    public static void main(String... args)
    throws SQLException, IOException
    {
        String jdbcUrl = "jdbc:h2:./test";

        Properties output = new Properties();

        try (Connection con = DriverManager.getConnection(jdbcUrl))
        {
            output.setProperty("db.productName", con.getMetaData().getDatabaseProductName());

            readCollationList(con, output);

            //This exercises collators, but Graal doesn't support I18n properly at the moment
            /*
            //Display list of locales available in the JVM along with rule length to show if all collators are actually the same or not
            for (Locale locale : Collator.getAvailableLocales())
            {
                Collator c = Collator.getInstance(locale);
                if (c instanceof RuleBasedCollator)
                {
                    RuleBasedCollator r = (RuleBasedCollator)c;
                    System.out.println(locale + ": " + r + " - " + r.getRules().length());
                }
                else
                    System.out.println(locale + ": " + c);
            }
             */

            //Check if table exists, if not create and add data
            if (!tableExists(con, "CAT"))
            {
                try (PreparedStatement stat = con.prepareStatement("create table cat (name varchar(64) not null primary key, owner varchar(64) not null)"))
                {
                    stat.executeUpdate();
                    output.setProperty("table.created", "true");
                }

                try (PreparedStatement stat = con.prepareStatement("insert into cat (name, owner) values (?, ?)"))
                {
                    stat.setString(1, "Dinah-Kah");
                    stat.setString(2, "John Galah");
                    stat.executeUpdate();

                    stat.setString(1, "Janvier");
                    stat.setString(2, "Jenny Cockatoo");
                    stat.executeUpdate();
                }
            }

            List<String> cats = new ArrayList<>();

            try (PreparedStatement stat = con.prepareStatement("select name from cat order by name");
                 ResultSet rs = stat.executeQuery())
            {
                while (rs.next())
                {
                    cats.add(rs.getString(1));
                }
            }

            output.setProperty("cats", String.join(" ", cats));
        }

        output.store(System.out, null);
    }

    /**
     * Exercise reading collations, which will hit incomplete LocaleServiceProviderPool Graal substitution
     * (https://github.com/oracle/graal/issues/839).
     */
    private static void readCollationList(Connection con, Properties output)
    throws SQLException
    {
        List<String> localeNames = new ArrayList<>();
        try (PreparedStatement stat = con.prepareStatement("select * from INFORMATION_SCHEMA.COLLATIONS");
             ResultSet rs = stat.executeQuery())
        {
            while (rs.next())
            {
                localeNames.add(rs.getString("NAME"));
            }
        }
        output.setProperty("collations", String.join(" ", localeNames));
    }

    private static boolean tableExists(Connection con, String tableName)
    throws SQLException
    {
        try (ResultSet rs = con.getMetaData().getTables(null, null, tableName, null))
        {
            return rs.next();
        }
    }
}
