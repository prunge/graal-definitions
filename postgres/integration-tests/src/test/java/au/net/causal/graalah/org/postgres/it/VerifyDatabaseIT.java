package au.net.causal.graalah.org.postgres.it;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class VerifyDatabaseIT
{
    private static final Properties dataProperties = new Properties();

    @BeforeAll
    private static void setUpDataProperties()
    throws IOException
    {
        Path dataFile = Paths.get(System.getProperty("data.file", "target/out-java.properties"));
        System.out.println("Verifying " + dataFile.toAbsolutePath().toString());
        dataProperties.clear();
        try (InputStream is = Files.newInputStream(dataFile))
        {
            dataProperties.load(is);
        }
    }

    @Test
    void dataFileHasCorrectContent()
    {
        assertThat(dataProperties).containsEntry("db.productName", "PostgreSQL");
        assertThat(dataProperties).containsEntry("cats", "Dinah-Kah Janvier");
    }
}
