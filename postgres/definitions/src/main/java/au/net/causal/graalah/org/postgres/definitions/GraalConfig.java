package au.net.causal.graalah.org.postgres.definitions;

import org.graalvm.nativeimage.hosted.Feature;
import org.graalvm.nativeimage.hosted.RuntimeClassInitialization;
import org.postgresql.sspi.SSPIClient;

public abstract class GraalConfig implements Feature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        //Uses JNA on Windows but not needed on other OSes and just causes problems
        RuntimeClassInitialization.initializeAtRunTime(SSPIClient.class);
    }
}
