package au.net.causal.graalah.org.postgres.feature;

import au.net.causal.graalah.org.postgres.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
}
