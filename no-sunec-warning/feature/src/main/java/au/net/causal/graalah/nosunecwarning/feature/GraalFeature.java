package au.net.causal.graalah.nosunecwarning.feature;

import au.net.causal.graalah.nosunecwarning.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
}
