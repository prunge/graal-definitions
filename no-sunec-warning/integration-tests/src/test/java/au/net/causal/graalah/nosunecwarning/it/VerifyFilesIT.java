package au.net.causal.graalah.nosunecwarning.it;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class VerifyFilesIT
{
    private static final List<String> executionOutputLines = new ArrayList<>();

    @BeforeAll
    private static void setUpFiles()
    throws IOException
    {
        Path dataFile = Paths.get(System.getProperty("data.file", "target/out-java.txt"));
        System.out.println("Verifying " + dataFile.toAbsolutePath().toString());
        executionOutputLines.clear();
        executionOutputLines.addAll(Files.readAllLines(dataFile));
    }

    @Test
    void executionOutputLinesHaveCorrectContent()
    {
        assertThat(executionOutputLines).containsExactly(
          "Algorithm:",
          "AES/ECB/NoPadding"
        );
    }
}
