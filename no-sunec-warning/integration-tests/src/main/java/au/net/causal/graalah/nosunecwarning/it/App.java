package au.net.causal.graalah.nosunecwarning.it;

import javax.crypto.Cipher;

public class App
{
    public static void main(String... args)
    throws Exception
    {
        //This just prints out the algorithm we looked up, but it also causes the cipher to be initialized and
        //also Provider.checkInitialized() to be called, triggering the SunEC warning if the library was not loaded
        System.out.println("Algorithm:");
        Cipher c = Cipher.getInstance("AES/ECB/NoPadding");
        System.out.println(c.getAlgorithm());
    }
}
