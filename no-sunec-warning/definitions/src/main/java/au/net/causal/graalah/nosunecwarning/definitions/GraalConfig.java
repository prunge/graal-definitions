package au.net.causal.graalah.nosunecwarning.definitions;

import com.oracle.svm.core.log.Log;
import org.graalvm.nativeimage.hosted.Feature;

public abstract class GraalConfig implements Feature
{
    @Override
    public void duringSetup(DuringSetupAccess access)
    {
        Log.setLog(new SunECWarningIgnoredLog());
    }
}
