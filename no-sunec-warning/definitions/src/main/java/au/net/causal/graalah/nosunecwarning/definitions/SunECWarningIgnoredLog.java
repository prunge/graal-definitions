package au.net.causal.graalah.nosunecwarning.definitions;

import com.oracle.svm.core.log.Log;
import com.oracle.svm.core.log.RealLog;

import java.util.concurrent.atomic.AtomicBoolean;

public class SunECWarningIgnoredLog extends RealLog
{
    private final AtomicBoolean warningWithoutNewline = new AtomicBoolean();

    @Override
    public Log string(String value)
    {
        //Hide specificall the SunEC warning but let through everything else
        if (value != null && value.startsWith("WARNING: The sunec native library, required by the SunEC provider, could not be loaded."))
        {
            warningWithoutNewline.set(true);
            return this;
        }

        return super.string(value);
    }

    @Override
    public Log newline()
    {
        //Suppress newline if there was a warning
        boolean actuallyHadWarning = warningWithoutNewline.compareAndSet(true, false);
        if (actuallyHadWarning)
            return this;

        return super.newline();
    }
}
