package au.net.causal.graalah.net.bramp.ffmpeg.feature;

import au.net.causal.graalah.net.bramp.ffmpeg.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
}
