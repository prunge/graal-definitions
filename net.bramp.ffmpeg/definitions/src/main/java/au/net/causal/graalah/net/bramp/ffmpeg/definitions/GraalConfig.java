package au.net.causal.graalah.net.bramp.ffmpeg.definitions;

import au.net.causal.graalah.tools.GraalReflection;
import au.net.causal.graalah.tools.ReflectionScanner;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import org.graalvm.nativeimage.hosted.Feature;

public abstract class GraalConfig implements Feature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        ReflectionScanner scanner = new ReflectionScanner.Builder().scanningAllContent().build();
        GraalReflection.with(scanner.scan(FFmpegProbeResult.class)).allMembers().register();
    }
}
