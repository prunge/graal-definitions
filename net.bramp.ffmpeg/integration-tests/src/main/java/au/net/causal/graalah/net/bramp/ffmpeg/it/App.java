package au.net.causal.graalah.net.bramp.ffmpeg.it;

import com.google.gson.Gson;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App
{
    private final FFprobe ffprobe;
    private final FFmpeg ffmpeg;

    private final Path mediaFile;

    public static void main(String... args)
    throws IOException
    {
        //Configure logging - ffmpeg logs noisily by default
        Logger.getLogger("net.bramp.ffmpeg").setLevel(Level.WARNING);

        Path mediaFile = Paths.get(args[0]);
        new App(mediaFile).run();
    }

    public App(Path mediaFile)
    throws IOException
    {
        this.ffprobe = new FFprobe();
        this.ffmpeg = new FFmpeg();
        this.mediaFile = Objects.requireNonNull(mediaFile);
    }

    public void run()
    throws IOException
    {
        FFmpegProbeResult result = ffprobe.probe(mediaFile.toAbsolutePath().toString());
        System.out.println(new Gson().toJson(result));
    }
}
