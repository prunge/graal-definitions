package au.net.causal.graalah.net.bramp.ffmpeg.it;

import com.google.gson.Gson;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.bramp.ffmpeg.probe.FFmpegStream.CodecType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.*;

class VerifyFilesIT
{
    private static Path javaProbeFile;
    private static Path graalProbeFile;

    @BeforeAll
    private static void setUpFiles()
    {
        javaProbeFile = Paths.get(System.getProperty("java.probe.file", "target/probe-java.json"));
        graalProbeFile = Paths.get(System.getProperty("graal.probe.file", "target/probe-graal.json"));

        assertThat(javaProbeFile).isRegularFile();
        assertThat(graalProbeFile).isRegularFile();
    }

    /**
     * If Graal reflection is configured property, GSON should read the JSON from FFMPEG exactly the same when running
     * from either SubstrateVM or from plain old Java.
     */
    @Test
    void javaAndGraalFilesHaveSameContent()
    {
        assertThat(graalProbeFile).hasSameContentAs(javaProbeFile, StandardCharsets.UTF_8);
    }

    @Test
    void verifyJavaProbeData()
    throws IOException
    {
        verifyProbeData(javaProbeFile);
    }

    @Test
    void verifyGraalProbeData()
    throws IOException
    {
        verifyProbeData(graalProbeFile);
    }

    private void verifyProbeData(Path file)
    throws IOException
    {
        try (BufferedReader reader = Files.newBufferedReader(file))
        {
            FFmpegProbeResult result = new Gson().fromJson(reader, FFmpegProbeResult.class);
            assertThat(result.getFormat().duration).isCloseTo(3.0, within(0.01));
            assertThat(result.getFormat().format_name).isEqualTo("matroska,webm");
            assertThat(result.getFormat().tags).containsEntry("ENCODER", "Lavf58.12.100");
            assertThat(result.getStreams()).hasSize(1);
            assertThat(result.getStreams().get(0).codec_name).isEqualTo("vp8");
            assertThat(result.getStreams().get(0).codec_type).isEqualTo(CodecType.VIDEO);
            assertThat(result.getStreams().get(0).avg_frame_rate.doubleValue()).isCloseTo(1.0, within(0.01));
        }
    }
}
