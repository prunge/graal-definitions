package au.net.causal.graalah.xerces.it;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.xmlunit.assertj.XmlAssert.assertThat;

class VerifyFilesIT
{
    private static final Properties dataProperties = new Properties();

    @BeforeAll
    private static void setUpDataProperties()
    throws IOException
    {
        Path dataFile = Paths.get(System.getProperty("data.file", "target/out-java.properties"));
        System.out.println("Verifying " + dataFile.toAbsolutePath().toString());
        dataProperties.clear();
        try (InputStream is = Files.newInputStream(dataFile))
        {
            dataProperties.load(is);
        }
    }

    /**
     * Use DOM API to read XML.
     */
    @Test
    void domReading()
    {
        assertThat(dataProperties.getProperty("dom.cat.name")).isEqualTo("Dinah-Kah");
        assertThat(dataProperties.getProperty("dom.cat.owner")).isEqualTo("John Galah");
    }

    /**
     * Use SAX API to read XML.
     */
    @Test
    void saxReading()
    {
        assertThat(dataProperties.getProperty("sax.cat.name")).isEqualTo("Dinah-Kah");
        assertThat(dataProperties.getProperty("sax.cat.owner")).isEqualTo("John Galah");
    }

    /**
     * JAXP validation API both success and failure cases.
     */
    @Test
    void schemaValidation()
    {
        //Check that the successful validation test actually passed validation
        assertThat(dataProperties.getProperty("validate.success")).isEqualTo("true");

        //Check that the validation failure was picked up properly
        assertThat(dataProperties.getProperty("validate.failure.error")).asString().contains("cvc-enumeration-valid");
    }
}
