package au.net.causal.graalah.xerces.it;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.stream.Collectors;

public class App
{
    private static final String schemaXsd = readResource("bird.xsd");

    public static void main(String... args)
    throws IOException, ParserConfigurationException, SAXException
    {
        Properties output = new Properties();

        dom(output);
        sax(output);
        validateSuccess(output);
        validateFailure(output);

        //Write properties data out
        output.store(System.out, null);
    }

    private static String readResource(String resourceName)
    {
        URL resourceUrl = App.class.getResource("/" + resourceName);
        try (BufferedReader r = new BufferedReader(new InputStreamReader(resourceUrl.openStream(), StandardCharsets.UTF_8)))
        {
            return r.lines().collect(Collectors.joining("\n"));
        }
        catch (IOException e)
        {
            throw new IOError(e);
        }
    }

    private static void dom(Properties output)
    throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        String xml = "<cats xmlns='graaltest:dom'><cat name='Dinah-Kah' owner='John Galah' /></cats>";

        //Parse from string
        Document doc = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));

        //Read elements using DOM
        NodeList catNodeList = doc.getDocumentElement().getElementsByTagNameNS("graaltest:dom", "cat");
        for (int i = 0; i < catNodeList.getLength(); i++)
        {
            Element catElement = (Element)catNodeList.item(i);
            String catName = catElement.getAttribute("name");
            String catOwner = catElement.getAttribute("owner");
            output.setProperty("dom.cat.name", catName);
            output.setProperty("dom.cat.owner", catOwner);
        }
    }

    private static void sax(Properties output)
    throws ParserConfigurationException, SAXException, IOException
    {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser parser = spf.newSAXParser();

        String xml = "<cats xmlns='graaltest:dom'><cat name='Dinah-Kah' owner='John Galah' /></cats>";

        parser.parse(new InputSource(new StringReader(xml)), new DefaultHandler()
        {
            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes)
            {
                if ("graaltest:dom".equals(uri) && "cat".equals(localName))
                {
                    output.setProperty("sax.cat.name", attributes.getValue("name"));
                    output.setProperty("sax.cat.owner", attributes.getValue("owner"));
                }
            }
        });
    }

    private static void validateSuccess(Properties output)
    throws SAXException, IOException
    {
        String xml = "<bird xmlns='http://www.causal.net.au/graal-definitions/it/xerces/bird'><name>Squinky</name><species>galah</species></bird>";

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new StreamSource(new StringReader(schemaXsd)));
        schema.newValidator().validate(new StreamSource(new StringReader(xml)));

        output.setProperty("validate.success", "true");
    }

    private static void validateFailure(Properties output)
    throws SAXException, IOException
    {
        String xml = "<bird xmlns='http://www.causal.net.au/graal-definitions/it/xerces/bird'><name>Dumbo</name><species>elephant</species></bird>";

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new StreamSource(new StringReader(schemaXsd)));
        try
        {
            schema.newValidator().validate(new StreamSource(new StringReader(xml)));
            throw new RuntimeException("XML should have failed validation");
        }
        catch (SAXParseException e)
        {
            //Expected validation failure
            output.setProperty("validate.failure.error", e.getMessage());
        }
    }
}
