package au.net.causal.graalah.xerces.definitions;

import au.net.causal.graalah.tools.ClassSelector;
import au.net.causal.graalah.tools.GraalReflection;
import com.oracle.svm.core.configure.ResourcesRegistry;
import org.apache.xerces.impl.dv.dtd.DTDDVFactoryImpl;
import org.apache.xerces.impl.dv.xs.SchemaDVFactoryImpl;
import org.apache.xerces.parsers.XIncludeAwareParserConfiguration;
import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.hosted.Feature;

import javax.xml.parsers.SAXParserFactory;

public abstract class GraalConfig implements Feature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        //Also XML supporting classes
        GraalReflection.with(ClassSelector.select(access).subclassesOf(SAXParserFactory.class).stream()).allMembers().register();

        GraalReflection.with(access.findClassByName("org.apache.xerces.parsers.ObjectFactory")).allMembers().register();
        GraalReflection.with(access.findClassByName("org.apache.xerces.impl.dv.ObjectFactory")).allMembers().register();

        GraalReflection.with(XIncludeAwareParserConfiguration.class).allConstructors().register();
        GraalReflection.with(DTDDVFactoryImpl.class).allConstructors().register();
        GraalReflection.with(SchemaDVFactoryImpl.class).allConstructors().register();

        addResourceBundle("org.apache.xerces.impl.msg.DatatypeMessages");
        addResourceBundle("org.apache.xerces.impl.msg.DOMMessages");
        addResourceBundle("org.apache.xerces.impl.msg.JAXPValidationMessages");
        addResourceBundle("org.apache.xerces.impl.msg.SAXMessages");
        addResourceBundle("org.apache.xerces.impl.msg.XIncludeMessages");
        addResourceBundle("org.apache.xerces.impl.msg.XMLMessages");
        addResourceBundle("org.apache.xerces.impl.msg.XMLSchemaMessages");
        addResourceBundle("org.apache.xerces.impl.msg.XMLSerializerMessages");
        addResourceBundle("org.apache.xerces.impl.msg.XPointerMessages");
    }

    protected void addResourceBundle(String baseName)
    {
        ImageSingletons.lookup(ResourcesRegistry.class).addResourceBundles(baseName);
    }
}
