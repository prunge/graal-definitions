package au.net.causal.graalah.xerces.feature;

import au.net.causal.graalah.xerces.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
}
