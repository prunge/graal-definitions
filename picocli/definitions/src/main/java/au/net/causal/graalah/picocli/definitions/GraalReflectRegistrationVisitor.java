package au.net.causal.graalah.picocli.definitions;

import com.oracle.svm.core.configure.ResourcesRegistry;
import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.hosted.RuntimeReflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ResourceBundle;

/**
 * Registers reflection-based access of Picocli commands with Graal.
 */
public class GraalReflectRegistrationVisitor extends BaseReflectionGeneratorVisitor {
    @Override
    protected RegisteredReflectedClass getOrCreateClass(Class<?> cls) {

        RuntimeReflection.register(cls);

        //All declared constructors
        RuntimeReflection.register(cls.getDeclaredConstructors());

        //All declared methods
        RuntimeReflection.register(cls.getDeclaredMethods());

        //Individual fields and methods are also registered via this returned object
        return new RegisteredReflectedClass();
    }

    protected static class RegisteredReflectedClass implements ReflectedClass {
        @Override
        public void addField(Field field) {
            RuntimeReflection.register(true, field);
        }

        @Override
        public void addMethod(Method method) {
            RuntimeReflection.register(method);
        }
    }

    @Override
    protected void addResourceBundle(ResourceBundle resourceBundle) {
        ImageSingletons.lookup(ResourcesRegistry.class).addResourceBundles(resourceBundle.getBaseBundleName());
    }
}
