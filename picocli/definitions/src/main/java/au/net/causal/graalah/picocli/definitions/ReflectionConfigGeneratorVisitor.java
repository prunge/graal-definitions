package au.net.causal.graalah.picocli.definitions;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Replicates what Picocli's original ReflectionConfigGenerator's visitor does by generating a JSON file
 * that is suitable for using with Graal.
 */
public class ReflectionConfigGeneratorVisitor extends BaseReflectionGeneratorVisitor {
    private static final String SYSPROP_CODEGEN_EXCLUDES = "picocli.codegen.excludes";

    private final Map<String, ConfigReflectedClass> visited = new TreeMap<String, ConfigReflectedClass>();

    @Override
    protected ConfigReflectedClass getOrCreateClass(Class<?> cls) {
        if (cls.isPrimitive()) {
            return new ConfigReflectedClass(cls.getName()); // don't store
        }
        return getOrCreateClassName(cls.getName());
    }

    private ConfigReflectedClass getOrCreateClassName(String name) {
        ConfigReflectedClass result = visited.get(name);
        if (result == null) {
            result = new ConfigReflectedClass(name);
            if (!excluded(name)) {
                visited.put(name, result);
            }
        }
        return result;
    }

    static boolean excluded(String fqcn) {
        String[] excludes = System.getProperty(SYSPROP_CODEGEN_EXCLUDES, "").split(",");
        for (String regex : excludes) {
            if (fqcn.matches(regex)) {
                System.err.printf("Class %s is excluded: (%s=%s)%n", fqcn, SYSPROP_CODEGEN_EXCLUDES, System.getProperty(SYSPROP_CODEGEN_EXCLUDES));
                return true;
            }
        }
        return false;
    }

    static class ConfigReflectedClass implements ReflectedClass {
        private final String name;
        private final Set<ReflectedField> fields = new TreeSet<ReflectedField>();
        private final Set<ReflectedMethod> methods = new TreeSet<ReflectedMethod>();

        ConfigReflectedClass(String name) {
            this.name = name;
        }

        @Override
        public void addField(Field f) {
            addField(f.getName(), isFinal(f));
        }

        protected ConfigReflectedClass addField(String fieldName, boolean isFinal) {
            fields.add(new ReflectedField(fieldName, isFinal));
            return this;
        }

        @Override
        public void addMethod(Method method) {
            addMethod(method.getName(), method.getParameterTypes());
        }

        private ReflectedClass addMethod0(String methodName, String... paramTypes) {
            methods.add(new ReflectedMethod(methodName, paramTypes));
            return this;
        }

        protected ReflectedClass addMethod(String methodName, Class... paramClasses) {
            String[] paramTypes = new String[paramClasses.length];
            for (int i = 0; i < paramClasses.length; i++) {
                paramTypes[i] = paramClasses[i].getName();
            }
            return addMethod0(methodName, paramTypes);
        }

        @Override
        public String toString() {
            String result = String.format("" +
                                                  "  {%n" +
                                                  "    \"name\" : \"%s\",%n" +
                                                  "    \"allDeclaredConstructors\" : true,%n" +
                                                  "    \"allPublicConstructors\" : true,%n" +
                                                  "    \"allDeclaredMethods\" : true,%n" +
                                                  "    \"allPublicMethods\" : true", name);
            if (!fields.isEmpty()) {
                result += String.format(",%n    \"fields\" : ");
                String prefix = String.format("[%n"); // start JSON array
                for (ReflectedField field : fields) {
                    result += String.format("%s      %s", prefix, field);
                    prefix = String.format(",%n");
                }
                result += String.format("%n    ]"); // end JSON array
            }
            if (!methods.isEmpty()) {
                result += String.format(",%n    \"methods\" : ");
                String prefix = String.format("[%n"); // start JSON array
                for (ReflectedMethod method : methods) {
                    result += String.format("%s      %s", prefix, method);
                    prefix = String.format(",%n");
                }
                result += String.format("%n    ]"); // end JSON array
            }
            result += String.format("%n  }");
            return result;
        }
    }
    static class ReflectedMethod implements Comparable<ReflectedMethod> {
        private final String name;
        private final String[] paramTypes;

        ReflectedMethod(String name, String... paramTypes) {
            this.name = name;
            this.paramTypes = paramTypes.clone();
        }
        @Override public int hashCode() { return name.hashCode() * Arrays.hashCode(paramTypes); }
        @Override public boolean equals(Object o) {
            return o instanceof ReflectedMethod
                    && ((ReflectedMethod) o).name.equals(name)
                    && Arrays.equals(((ReflectedMethod) o).paramTypes, paramTypes);
        }

        @Override
        public String toString() {
            return String.format("{ \"name\" : \"%s\", \"parameterTypes\" : [%s] }", name, formatParamTypes());
        }

        private String formatParamTypes() {
            StringBuilder result = new StringBuilder();
            for (String type : paramTypes) {
                if (result.length() > 0) {
                    result.append(", ");
                }
                result.append('"').append(type).append('"');
            }
            return result.toString();
        }

        public int compareTo(ReflectedMethod o) {
            int result = name.compareTo(o.name);
            if (result == 0) {
                result = Arrays.toString(this.paramTypes).compareTo(Arrays.toString(o.paramTypes));
            }
            return result;
        }
    }
    static class ReflectedField implements Comparable<ReflectedField> {
        private final String name;
        private final boolean isFinal;

        ReflectedField(String name, boolean isFinal) {
            this.name = name;
            this.isFinal = isFinal;
        }
        @Override public int hashCode() { return name.hashCode(); }
        @Override public boolean equals(Object o) {
            return o instanceof ReflectedField && ((ReflectedField) o).name.equals(name);
        }

        @Override
        public String toString() {
            return isFinal
                    ? String.format("{ \"name\" : \"%s\", \"allowWrite\" : true }", name)
                    : String.format("{ \"name\" : \"%s\" }", name);
        }

        public int compareTo(ReflectedField o) {
            return name.compareTo(o.name);
        }
    }

    @Override
    protected void addResourceBundle(ResourceBundle resourceBundle) {
        // Not handled by this generator
    }
}
