package au.net.causal.graalah.picocli.definitions;

import picocli.CommandLine;
import picocli.CommandLine.Model.ArgSpec;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Model.IGetter;
import picocli.CommandLine.Model.ISetter;
import picocli.CommandLine.Model.OptionSpec;
import picocli.CommandLine.Model.PositionalParamSpec;
import picocli.CommandLine.Model.UnmatchedArgsBinding;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ResourceBundle;

//Logic copied from Picocli codegen's ReflectionConfigGenerator
public abstract class BaseReflectionGeneratorVisitor {

    private static final String REFLECTED_FIELD_BINDING_CLASS = "picocli.CommandLine$Model$FieldBinding";
    private static final String REFLECTED_METHOD_BINDING_CLASS = "picocli.CommandLine$Model$MethodBinding";
    private static final String REFLECTED_FIELD_BINDING_FIELD = "field";
    private static final String REFLECTED_METHOD_BINDING_METHOD = "method";
    private static final String REFLECTED_BINDING_FIELD_SCOPE = "scope";

    /**
     * Register common classes that Picocli requires, regardless of the commands on the classpath.
     */
    public void registerCommons() throws NoSuchMethodException, NoSuchFieldException {
        //These cause problems when registered for reflection
        //See: https://github.com/oracle/graal/issues/1260
        //getOrCreateClass(Method.class);
        //addClassAndMethod(Executable.class.getMethod("getParameters"));
        //addClassAndMethod(Parameter.class.getMethod("getName"));

        // ANSI color enabled detection
        addClassAndMethod(System.class.getMethod("console"));
        getOrCreateClassByName("org.fusesource.jansi.AnsiConsole").addField("out");

        // picocli 4.0
        addClassAndMethod(ResourceBundle.class.getMethod("getBaseBundleName"));

        // type converters registered with reflection
        addClassAndMethod(Duration.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(Instant.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(LocalDate.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(LocalDateTime.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(LocalTime.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(MonthDay.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(OffsetDateTime.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(OffsetTime.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(Period.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(Year.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(YearMonth.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(ZonedDateTime.class.getMethod("parse", CharSequence.class));
        addClassAndMethod(ZoneId.class.getMethod("of", String.class));
        addClassAndMethod(ZoneOffset.class.getMethod("of", String.class));
        getOrCreateClass(Path.class);
        addClassAndMethod(Paths.class.getMethod("get", String.class, String[].class));

        getOrCreateClass(Connection.class);
        getOrCreateClass(Driver.class);
        addClassAndMethod(DriverManager.class.getMethod("getConnection", String.class));
        addClassAndMethod(DriverManager.class.getMethod("getDriver", String.class));
        addClassAndMethod(Timestamp.class.getMethod("valueOf", String.class));
    }

    public void visitCommandSpec(CommandSpec spec) throws NoSuchFieldException, IllegalAccessException {
        if (spec.userObject() != null) {
            if (spec.userObject() instanceof Method) {
                Method method = (Method) spec.userObject();
                ReflectedClass cls = getOrCreateClass(method.getDeclaringClass());
                cls.addMethod(method);
            } else if (Proxy.isProxyClass(spec.userObject().getClass())) {
                // do nothing
            } else {
                visitAnnotatedFields(spec.userObject().getClass());
            }
        }
        visitObjectType(spec.versionProvider());
        visitObjectType(spec.defaultValueProvider());

        for (UnmatchedArgsBinding binding : spec.unmatchedArgsBindings()) {
            visitGetter(binding.getter());
            visitSetter(binding.setter());
        }
        for (OptionSpec option : spec.options()) {
            visitArgSpec(option);
        }
        for (PositionalParamSpec positional : spec.positionalParameters()) {
            visitArgSpec(positional);
        }
        for (CommandSpec mixin : spec.mixins().values()) {
            visitCommandSpec(mixin);
        }
        for (CommandLine sub : spec.subcommands().values()) {
            visitCommandSpec(sub.getCommandSpec());
        }

        ResourceBundle resourceBundle = spec.resourceBundle();
        if (resourceBundle != null) {
            addResourceBundle(resourceBundle);
        }
    }

    private void visitAnnotatedFields(Class<?> cls) {
        if (cls == null) {
            return;
        }
        ReflectedClass reflectedClass = getOrCreateClass(cls);
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field f : declaredFields) {
            if (f.isAnnotationPresent(CommandLine.Spec.class)) {
                reflectedClass.addField(f);
            }
            if (f.isAnnotationPresent(CommandLine.ParentCommand.class)) {
                reflectedClass.addField(f);
            }
            if (f.isAnnotationPresent(CommandLine.Mixin.class)) {
                reflectedClass.addField(f);
            }
            if (f.isAnnotationPresent(CommandLine.Unmatched.class)) {
                reflectedClass.addField(f);
            }
        }
        visitAnnotatedFields(cls.getSuperclass());
    }

    private void visitGetter(IGetter getter) throws NoSuchFieldException, IllegalAccessException {
        if (getter == null) {
            return;
        }
        if (REFLECTED_FIELD_BINDING_CLASS.equals(getter.getClass().getName())) {
            visitFieldBinding(getter);
        }
        if (REFLECTED_METHOD_BINDING_CLASS.equals(getter.getClass().getName())) {
            visitMethodBinding(getter);
        }
    }

    private void visitSetter(ISetter setter) throws NoSuchFieldException, IllegalAccessException {
        if (setter == null) {
            return;
        }
        if (REFLECTED_FIELD_BINDING_CLASS.equals(setter.getClass().getName())) {
            visitFieldBinding(setter);
        }
        if (REFLECTED_METHOD_BINDING_CLASS.equals(setter.getClass().getName())) {
            visitMethodBinding(setter);
        }
    }

    private void visitFieldBinding(Object fieldBinding) throws IllegalAccessException, NoSuchFieldException {
        Field field = (Field) accessibleField(fieldBinding.getClass(), REFLECTED_FIELD_BINDING_FIELD).get(fieldBinding);
        getOrCreateClass(field.getDeclaringClass())
                .addField(field);

        Object scope = accessibleField(fieldBinding.getClass(), REFLECTED_BINDING_FIELD_SCOPE).get(fieldBinding);
        getOrCreateClass(scope.getClass());
    }

    private void visitMethodBinding(Object methodBinding) throws IllegalAccessException, NoSuchFieldException {
        Method method = (Method) accessibleField(methodBinding.getClass(), REFLECTED_METHOD_BINDING_METHOD).get(methodBinding);
        ReflectedClass cls = getOrCreateClass(method.getDeclaringClass());
        cls.addMethod(method);

        Object scope = accessibleField(methodBinding.getClass(), REFLECTED_BINDING_FIELD_SCOPE).get(methodBinding);
        ReflectedClass scopeClass = getOrCreateClass(scope.getClass());
        if (!scope.getClass().equals(method.getDeclaringClass())) {
            scopeClass.addMethod(method);
        }
    }

    private void visitTypes(Class<?>[] classes) {
        for (Class<?> cls : classes) { visitType(cls); }
    }

    private void visitType(Class<?> type) {
        if (type != null) { getOrCreateClass(type); }
    }

    private void visitObjectType(Object object) {
        if (object != null) { visitType(object.getClass()); }
    }

    private void visitObjectTypes(Object[] array) {
        if (array != null) {
            for (Object element : array) { visitObjectType(element); }
        }
    }

    protected static boolean isFinal(Field f) {
        return Modifier.isFinal(f.getModifiers());
    }

    private void visitArgSpec(ArgSpec argSpec) throws NoSuchFieldException, IllegalAccessException {
        visitGetter(argSpec.getter());
        visitSetter(argSpec.setter());
        visitType(argSpec.type());
        visitTypes(argSpec.auxiliaryTypes());
        visitObjectType(argSpec.completionCandidates());
        visitObjectTypes(argSpec.converters());
    }

    protected static Field accessibleField(Class<?> cls, String fieldName) throws NoSuchFieldException {
        Field field = cls.getDeclaredField(fieldName);
        field.setAccessible(true);
        return field;
    }

    protected abstract ReflectedClass getOrCreateClass(Class<?> cls);
    protected abstract void addResourceBundle(ResourceBundle resourceBundle);

    /**
     * Registers a class by name if it exists.  Use this if the class may not exist on the classpath.
     *
     * @param name the class binary name.
     *
     * @return the reflected class.  Implementation will be a no-op if the class was not found.  Never returns null.
     */
    protected ReflectedClassByName getOrCreateClassByName(String name) {
        try {
            Class<?> clazz = Class.forName(name);
            return new ExistingReflectedClass(clazz);
        } catch (ClassNotFoundException e) {
            //System.out.println("Could not find class: " + name);
            return new NotFoundReflectedClass();
        }
    }

    protected void addClassAndMethod(Method method) {
        getOrCreateClass(method.getDeclaringClass()).addMethod(method);
    }

    protected static interface ReflectedClass {
        void addField(Field field);
        void addMethod(Method method);
    }

    protected static interface ReflectedClassByName {
        void addField(String fieldName) throws NoSuchFieldException;
    }

    private class ExistingReflectedClass implements ReflectedClassByName {
        private final Class<?> clazz;
        private final ReflectedClass reflectedClass;

        public ExistingReflectedClass(Class<?> clazz) {
            this.clazz = clazz;
            this.reflectedClass = getOrCreateClass(clazz);
        }

        @Override
        public void addField(String fieldName) throws NoSuchFieldException {
            reflectedClass.addField(this.clazz.getDeclaredField(fieldName));
        }
    }

    /**
     * Represents a class that could not be found by name.  All methods are no-ops.
     */
    private static class NotFoundReflectedClass implements ReflectedClassByName
    {
        @Override
        public void addField(String fieldName) {
        }
    }
}
