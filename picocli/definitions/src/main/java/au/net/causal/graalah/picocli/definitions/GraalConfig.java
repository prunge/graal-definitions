package au.net.causal.graalah.picocli.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import au.net.causal.graalah.tools.ClassSelector;
import au.net.causal.graalah.tools.SubstitutionFeature;
import org.graalvm.nativeimage.hosted.Feature;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;
import picocli.CommandLine.Spec;
import picocli.CommandLine.Unmatched;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class GraalConfig implements Feature, SubstitutionFeature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        processPicocliClasses(
                ClassSelector.select(access)
                             .classesWithAnyMembersAnnotatedWith(Option.class, Parameters.class, Command.class,
                                                                 ParentCommand.class, Mixin.class, Spec.class,
                                                                 Unmatched.class)
                             .stream());
    }

    /**
     * Override to customize the reflection visitor.  By default returns an instance of
     * {@link GraalReflectRegistrationVisitor}.
     */
    protected BaseReflectionGeneratorVisitor createReflectionVisitor()
    {
        return new GraalReflectRegistrationVisitor();
    }

    /**
     * Given some Picocli classes that were detected on the classpath, process them with the reflection visitor
     * that will register their details with Graal.
     *
     * @param classes a stream of Picocli-using classes that should be processed.
     */
    private void processPicocliClasses(Stream<Class<?>> classes)
    {
        BaseReflectionGeneratorVisitor visitor = createReflectionVisitor();
        try
        {
            visitor.registerCommons();
        }
        catch (ReflectiveOperationException e)
        {
            throw new RuntimeException(e);
        }

        List<Class<?>> allPicocliClasses = classes.collect(Collectors.toList());

        //Command annotated classes
        allPicocliClasses
                .stream()
                .filter(clazz -> clazz.isAnnotationPresent(Command.class))
                .map(clazz -> new CommandLine(clazz).getCommandSpec())
               .forEach(spec ->
               {
                   try
                   {
                       visitor.visitCommandSpec(spec);
                   }
                   catch (ReflectiveOperationException e)
                   {
                       throw new RuntimeException("Error processing spec " + spec + ": " + e, e);
                   }
               });

        //Methods annotated with command
        allPicocliClasses
                .stream()
                .flatMap(this::commandMethods)
                .map(method -> new CommandLine(method).getCommandSpec())
                .forEach(spec ->
                {
                    try
                    {
                        visitor.visitCommandSpec(spec);
                    }
                    catch (ReflectiveOperationException e)
                    {
                        throw new RuntimeException("Error processing spec " + spec + ": " + e, e);
                    }
                });
    }

    private Stream<Method> commandMethods(Class<?> clazz)
    {
        return Stream.of(clazz.getDeclaredMethods())
                     .filter(method -> method.isAnnotationPresent(Command.class));
    }

    @Override
    public boolean isSubstitutionClassEnabled(Class<?> substitutionClass, String originalClassName)
    {
        if (substitutionClass == Ansi.class)
            return isSubstitutionEnabledForAnsi();
        else if (substitutionClass == Interpreter.class)
            return isSubstitutionEnabledForInterpreter();
        else if (substitutionClass == MethodParam.class)
            return isSubstitutionEnabledForMethodParam();
        else if (substitutionClass == CommandSpec.class)
            return isSubstitutionEnabledForCommandSpec();
        else if (substitutionClass == Model.class)
            return isSubstitutionEnabledForModel();
        else
            return false;
    }

    /**
     * Opt-in/out for substitution class.
     *
     * @see picocli.CommandLine.Help.Ansi
     */
    protected abstract boolean isSubstitutionEnabledForAnsi();

    /**
     * Opt-in/out for substitution class.
     *
     * @see picocli.CommandLine picocli.CommandLine.Interpreter
     */
    protected abstract boolean isSubstitutionEnabledForInterpreter();

    /**
     * Opt-in/out for substitution class.
     *
     * @see picocli.CommandLine.Model picocli.CommandLine.Model.MethodParam
     */
    protected abstract boolean isSubstitutionEnabledForMethodParam();

    /**
     * Opt-in/out for substitution class.
     *
     * @see picocli.CommandLine.Model.CommandSpec
     */
    protected abstract boolean isSubstitutionEnabledForCommandSpec();

    /**
     * Opt-in/out for substitution class.
     *
     * @see picocli.CommandLine.Model
     */
    protected abstract boolean isSubstitutionEnabledForModel();

    @Override
    public void afterRegistration(AfterRegistrationAccess access)
    {
        BaseSubstitutionChecker.registerSubstitutionFeature(this);
    }
}
