package au.net.causal.graalah.picocli.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;
import picocli.CommandLine.Model;

import java.lang.reflect.Method;

@TargetClass(value=Model.class, innerClass="MethodParam", onlyWith = MethodParam.Checker.class)
public final class MethodParam
{
    @Alias Method method;
    @Alias int paramIndex;
    @Alias String name;

    @Substitute
    public MethodParam(Method method, int paramIndex)
    {
        this.method = method;
        this.paramIndex = paramIndex;
        this.name = method.getParameters()[paramIndex].getName();
    }

    public static class Checker extends BaseSubstitutionChecker<MethodParam>
    {
        public Checker()
        {
            super(MethodParam.class);
        }
    }
}
