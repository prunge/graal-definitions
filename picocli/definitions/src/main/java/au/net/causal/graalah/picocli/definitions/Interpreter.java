package au.net.causal.graalah.picocli.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;
import picocli.CommandLine;

@TargetClass(value=CommandLine.class, innerClass="Interpreter", onlyWith = Interpreter.Checker.class)
public final class Interpreter
{
    @Substitute
    char[] readPassword(String prompt)
    {
        return System.console().readPassword(prompt);
    }

    public static class Checker extends BaseSubstitutionChecker<Interpreter>
    {
        public Checker()
        {
            super(Interpreter.class);
        }
    }
}
