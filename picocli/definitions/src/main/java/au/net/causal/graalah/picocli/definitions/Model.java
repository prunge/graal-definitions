package au.net.causal.graalah.picocli.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.TargetClass;
import picocli.CommandLine;

/**
 * Purely exist so we can access inNonDefault() by alias in another substitution class.  Doesn't actually
 * change any behaviour at all.
 */
@TargetClass(value = CommandLine.Model.class, onlyWith = Model.Checker.class)
public final class Model {
    @Alias
    public static boolean isNonDefault(Object candidate, Object defaultValue) {
        throw new Error("Alias only");
    }

    public static class Checker extends BaseSubstitutionChecker<Model>
    {
        public Checker()
        {
            super(Model.class);
        }
    }
}
