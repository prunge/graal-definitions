package au.net.causal.graalah.picocli.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;
import picocli.CommandLine;

@TargetClass(value = CommandLine.Help.Ansi.class, onlyWith = Ansi.Checker.class)
public final class Ansi
{
    @Substitute
    public static boolean calcTTY()
    {
        //Avoid using reflection since we know we're on at least Java 8
        return System.console() != null;
    }

    public static class Checker extends BaseSubstitutionChecker<Ansi>
    {
        public Checker()
        {
            super(Ansi.class);
        }
    }
}
