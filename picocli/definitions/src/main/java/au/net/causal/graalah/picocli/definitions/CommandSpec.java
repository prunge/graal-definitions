package au.net.causal.graalah.picocli.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.Inject;
import com.oracle.svm.core.annotate.RecomputeFieldValue;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;
import org.graalvm.nativeimage.ProcessProperties;
import picocli.CommandLine;

import java.nio.file.Paths;

import static au.net.causal.graalah.picocli.definitions.Model.isNonDefault;

@TargetClass(value = CommandLine.Model.CommandSpec.class, onlyWith = CommandSpec.Checker.class)
public final class CommandSpec {

    @Alias static String DEFAULT_COMMAND_NAME;
    @Alias private String name;
    @Alias private CommandSpec parent;

    /**
     * True if the command name was configured from an annotation or something, false if it was derived from
     * the method name.
     */
    @Inject
    @RecomputeFieldValue(kind = RecomputeFieldValue.Kind.Reset)
    private boolean commandNameConfigured;

    /**
     * This version of qualifiedName() with zero arguments (as opposed to the other one that takes an arg)
     * is only used for generating the executable name in the help text, so substitute to use the
     * executable name if we detect the command spec doesn't define its own name from an annotation.
     *
     * Be aware the other one is used for more stuff like looking up keys in bundles, so this approach is quite
     * flakey but the best I could come up given what is there.
     */
    @Substitute
    public String qualifiedName() {
        String q = qualifiedName(" ");
        if (!commandNameConfigured || DEFAULT_COMMAND_NAME.equals(q)) {
            q = Paths.get(ProcessProperties.getExecutableName()).getFileName().toString();
        }
        return q;
    }

    @Alias
    public String qualifiedName(String separator) {
        throw new Error("Alias only");
    }

    /**
     * Keep track of whether a name was updated from an annotation.
     */
    @Substitute
    void updateName(String value) {
        if (isNonDefault(value, DEFAULT_COMMAND_NAME)) {
            name = value;

            //Keep track of whether the name was updated from configuration
            commandNameConfigured = true;
        }
    }

    public static class Checker extends BaseSubstitutionChecker<CommandSpec>
    {
        public Checker()
        {
            super(CommandSpec.class);
        }
    }
}