package au.net.causal.graalah.picocli.feature;

import au.net.causal.graalah.picocli.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
    @Override
    protected boolean isSubstitutionEnabledForAnsi()
    {
        return true;
    }

    @Override
    protected boolean isSubstitutionEnabledForInterpreter()
    {
        return true;
    }

    @Override
    protected boolean isSubstitutionEnabledForMethodParam()
    {
        return true;
    }

    @Override
    protected boolean isSubstitutionEnabledForCommandSpec()
    {
        return true;
    }

    @Override
    protected boolean isSubstitutionEnabledForModel()
    {
        return true;
    }
}
