package au.net.causal.graalah.picocli.it;

import com.google.common.io.MoreFiles;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class VerifyFilesIT
{
    private static final Properties dataProperties = new Properties();
    private static String noArgsOutput;

    @BeforeAll
    private static void setUpDataProperties()
    throws IOException
    {
        Path dataFile = Paths.get(System.getProperty("data.file", "target/out-java.properties"));
        System.out.println("Verifying " + dataFile.toAbsolutePath().toString());
        dataProperties.clear();
        try (InputStream is = Files.newInputStream(dataFile))
        {
            dataProperties.load(is);
        }
    }

    @BeforeAll
    private static void setUpNoArgsOutput()
    throws IOException
    {
        Path noArgsOutputFile = Paths.get(System.getProperty("data.file.noargs", "target/out-noargs-java.txt"));
        System.out.println("Verifying " + noArgsOutputFile.toAbsolutePath().toString());
        noArgsOutput = MoreFiles.asCharSource(noArgsOutputFile, StandardCharsets.UTF_8).read();
    }

    @Test
    void dataFileHasCorrectContent()
    {
        assertThat(dataProperties).contains(entry("name", "John"));
        assertThat(dataProperties).contains(entry("date.year", "1977"));
        assertThat(dataProperties).contains(entry("date.month", "1"));
        assertThat(dataProperties).contains(entry("date.day", "9"));
        assertThat(dataProperties).contains(entry("birdType", "galah"));
        assertThat(dataProperties).contains(entry("number", "75"));
    }

    @Test
    void resourceBundleBasedCommandsGenerateHelpText()
    {
        //This string is read from a resource bundle and is displayed in the help text
        assertThat(noArgsOutput).contains("Specify a name on the command line");
    }

    /**
     * When running as a Graal executable, the command name is set to the executable name.
     * Otherwise it just keeps the default of &lt;main class &gt;.
     */
    @Test
    void commandNameIsSetToExecutableName()
    {
        //Method command name defaults to method name, so if no system property configured the executable then assume Picocli default
        String executableName = System.getProperty("executable.name", "runIt");
        assertThat(noArgsOutput).contains("Usage: " + executableName);
    }
}
