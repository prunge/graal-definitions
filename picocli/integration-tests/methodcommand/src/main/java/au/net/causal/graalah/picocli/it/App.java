package au.net.causal.graalah.picocli.it;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;

public class App
{
    public static void main(String... args)
    {
        CommandLine.invoke("runIt", App.class, args);
    }

    @Command(resourceBundle = "au.net.causal.graalah.picocli.it.CmdResources")
    public void runIt(@Option(names="--name", descriptionKey="name.description", required=true) String name,
                      @Option(names="--date", description="A date", required=true) LocalDate date,
                      @Option(names="--bird", description="Bird type", required=true) BirdType birdType,
                      @Mixin MoreOptions more)
    throws IOException
    {
        Properties output = new Properties();

        output.setProperty("name", name);
        output.setProperty("date.year", String.valueOf(date.getYear()));
        output.setProperty("date.month", String.valueOf(date.getMonthValue()));
        output.setProperty("date.day", String.valueOf(date.getDayOfMonth()));
        output.setProperty("birdType", String.valueOf(birdType));
        output.setProperty("number", String.valueOf(more.number));

        output.store(System.out, null);
    }

    private enum BirdType
    {
        galah,
        cockatoo;
    }

    private static class MoreOptions
    {
        @Option(names="--number", description="A number", required=true)
        private int number;
    }
}
