package au.net.causal.graalah.picocli.it;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;
import picocli.CommandLine.Unmatched;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;

@Command(resourceBundle = "au.net.causal.graalah.picocli.it.CmdResources")
public class App implements Callable<Void>
{
    /**
     * A simple string option.
     */
    @Option(names="--name", descriptionKey="name.description", required=true)
    private String name;

    /**
     * A timestamp from Java time API to test that these classes are registered for reflection.
     */
    @Option(names="--date", description="A date", required=true)
    private LocalDate date;

    @Option(names="--bird", description="Bird type", required=true)
    private BirdType birdType;

    @Mixin
    private MoreOptions more;

    /**
     * Any other args.
     */
    @Unmatched
    private List<String> args = new ArrayList<>();

    public static void main(String... args)
    {
        CommandLine.call(new App(), args);
    }

    @Override
    public Void call()
    throws IOException
    {
        Properties output = new Properties();

        output.setProperty("name", name);
        output.setProperty("date.year", String.valueOf(date.getYear()));
        output.setProperty("date.month", String.valueOf(date.getMonthValue()));
        output.setProperty("date.day", String.valueOf(date.getDayOfMonth()));
        output.setProperty("birdType", String.valueOf(birdType));
        output.setProperty("others", args.toString());
        output.setProperty("number", String.valueOf(more.number));

        output.store(System.out, null);

        return null;
    }

    private enum BirdType
    {
        galah,
        cockatoo;
    }

    private static class MoreOptions
    {
        @Option(names="--number", description="A number", required=true)
        private int number;
    }
}
