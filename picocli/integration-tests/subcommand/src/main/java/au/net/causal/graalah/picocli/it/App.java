package au.net.causal.graalah.picocli.it;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.DefaultExceptionHandler;
import picocli.CommandLine.Option;
import picocli.CommandLine.RunLast;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;

@Command(resourceBundle = "au.net.causal.graalah.picocli.it.CmdResources")
public class App
{
    @Option(names="--name", descriptionKey="name.description", required=true)
    private String name;

    @Option(names="--date", description="A date", required=true)
    private LocalDate date;

    public static void main(String... args)
    {
        new CommandLine(App.class).parseWithHandlers(new RunLast(), new DefaultExceptionHandler<>(), args);
    }

    @Command
    public void air(@Option(names="--airbird", description="Air bird type", required=true) AirBirdType airBirdType)
    throws IOException
    {
        Properties output = new Properties();

        output.setProperty("name", name);
        output.setProperty("date.year", String.valueOf(date.getYear()));
        output.setProperty("date.month", String.valueOf(date.getMonthValue()));
        output.setProperty("date.day", String.valueOf(date.getDayOfMonth()));
        output.setProperty("airBirdType", String.valueOf(airBirdType));

        output.store(System.out, null);
    }

    @Command
    public void water(@Option(names="--waterbird", description="Water bird type", required=true) WaterBirdType waterBirdType)
    throws IOException
    {
        Properties output = new Properties();

        output.setProperty("name", name);
        output.setProperty("date.year", String.valueOf(date.getYear()));
        output.setProperty("date.month", String.valueOf(date.getMonthValue()));
        output.setProperty("date.day", String.valueOf(date.getDayOfMonth()));
        output.setProperty("waterBirdType", String.valueOf(waterBirdType));

        output.store(System.out, null);
    }

    private enum AirBirdType
    {
        galah,
        cockatoo;
    }

    private enum WaterBirdType
    {
        duck,
        swan;
    }
}
