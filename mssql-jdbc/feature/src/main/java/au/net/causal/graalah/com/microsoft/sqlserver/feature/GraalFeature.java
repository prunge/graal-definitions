package au.net.causal.graalah.com.microsoft.sqlserver.feature;

import au.net.causal.graalah.com.microsoft.sqlserver.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
}
