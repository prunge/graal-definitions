package au.net.causal.graalah.com.microsoft.sqlserver.it;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class App
{
    public static void main(String... args)
    throws SQLException, IOException
    {
        String jdbcUrl = args[0];
        String user = args[1];
        String password = args[2];

        Properties output = new Properties();

        try (Connection con = DriverManager.getConnection(jdbcUrl, user, password))
        {
            output.setProperty("db.productName", con.getMetaData().getDatabaseProductName());

            List<String> cats = new ArrayList<>();

            try (PreparedStatement stat = con.prepareStatement("select name from cat order by name");
                 ResultSet rs = stat.executeQuery())
            {
                while (rs.next())
                {
                    cats.add(rs.getString(1));
                }
            }

            output.setProperty("cats", String.join(" ", cats));
        }

        output.store(System.out, null);
    }
}
