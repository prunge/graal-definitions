package au.net.causal.graalah.com.microsoft.sqlserver.definitions;

import com.microsoft.sqlserver.jdbc.SQLServerResource;
import com.oracle.svm.core.configure.ResourcesRegistry;
import com.oracle.svm.core.jdk.LocalizationFeature;
import org.graalvm.nativeimage.ImageSingletons;
import org.graalvm.nativeimage.hosted.Feature;

import java.nio.charset.Charset;

public abstract class GraalConfig implements Feature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        addResourceBundle(SQLServerResource.class.getName());
        configureCharsets();
    }

    protected void addResourceBundle(String baseName)
    {
        ImageSingletons.lookup(ResourcesRegistry.class).addResourceBundles(baseName);
    }

    protected void configureCharsets()
    {
        //See SQLCollation.Encoding for list of SQL Server charsets
        //Looks like we only need this one to get the driver basically running
        //but if the user needs other ones they can add more themselves
        LocalizationFeature.addCharset(Charset.forName("Cp1252"));
    }
}
