package au.net.causal.graalah.org.mariadb.feature;

import au.net.causal.graalah.org.mariadb.definitions.GraalConfig;
import com.oracle.svm.core.annotate.AutomaticFeature;

@AutomaticFeature
public class GraalFeature extends GraalConfig
{
    @Override
    protected boolean isSubstitutionEnabledForSendPamAuthPacket()
    {
        return true;
    }
}
