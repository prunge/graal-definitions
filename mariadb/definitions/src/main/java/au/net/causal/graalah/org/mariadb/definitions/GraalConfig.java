package au.net.causal.graalah.org.mariadb.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import au.net.causal.graalah.tools.GraalReflection;
import au.net.causal.graalah.tools.SubstitutionFeature;
import org.graalvm.nativeimage.hosted.Feature;
import org.mariadb.jdbc.internal.util.Options;

public abstract class GraalConfig implements Feature, SubstitutionFeature
{
    @Override
    public void beforeAnalysis(BeforeAnalysisAccess access)
    {
        GraalReflection.with(Options.class).allFields().register();
    }

    @Override
    public boolean isSubstitutionClassEnabled(Class<?> substitutionClass, String originalClassName)
    {
        if (substitutionClass == SendPamAuthPacket.class)
            return isSubstitutionEnabledForSendPamAuthPacket();
        else
            return false;
    }

    /**
     * Opt-in/out for substitution class.
     *
     * @see SendPamAuthPacket
     */
    protected abstract boolean isSubstitutionEnabledForSendPamAuthPacket();

    @Override
    public void afterRegistration(AfterRegistrationAccess access)
    {
        BaseSubstitutionChecker.registerSubstitutionFeature(this);
    }
}
