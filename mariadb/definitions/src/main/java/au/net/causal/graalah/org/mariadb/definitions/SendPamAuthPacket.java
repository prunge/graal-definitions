package au.net.causal.graalah.org.mariadb.definitions;

import au.net.causal.graalah.tools.BaseSubstitutionChecker;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

import java.io.Console;
import java.io.IOException;

@TargetClass(value=org.mariadb.jdbc.internal.com.send.SendPamAuthPacket.class, onlyWith=SendPamAuthPacket.Checker.class)
public final class SendPamAuthPacket {
    @Substitute
    private String showInputDialog(String label, boolean isPassword) throws IOException {
        String password;

        Console console = System.console();
        if (console == null) {
            throw new IOException("Error during PAM authentication : input by console not possible");
        }
        if (isPassword) {
            char[] passwordChar = console.readPassword(label);
            password = new String(passwordChar);
        } else {
            password = console.readLine(label);
        }

        if (password != null) {
            return password;
        } else {
            throw new IOException("Error during PAM authentication : dialog input cancelled");
        }
    }

    public static class Checker extends BaseSubstitutionChecker<SendPamAuthPacket> {
        public Checker() {
            super(SendPamAuthPacket.class);
        }
    }
}
